-- --------------------------------------------------------
-- --------------------------------------------------------
-- Train Ticket Booking Manager
--
-- Database initialisation
--
--
--
-- Thomas Pucci <thomas.pucci@ecl13.ec-lyon.fr>
-- Yann Vaginay <yann.vaginay@ecl13.ec-lyon.fr>
-- 
-- --------------------------------------------------------
-- --------------------------------------------------------

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `trains_BE_2016`
--
CREATE DATABASE `trains_BE_2016` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `trains_BE_2016`;

-- --------------------------------------------------------

--
-- Structure de la table `classes`
--

CREATE TABLE `classes` (
  `num` smallint(5) UNSIGNED NOT NULL,
  `price_rate` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `coaches`
--

CREATE TABLE `coaches` (
  `id` int(10) UNSIGNED NOT NULL,
  `num_class` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `coach_seats`
--

CREATE TABLE `coach_seats` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_coach` int(10) UNSIGNED NOT NULL,
  `seat_num` smallint(5) UNSIGNED NOT NULL,
  `situation` enum('window','aisle') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `birthdate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Structure de la table `customer_reductioncard`
--

CREATE TABLE `customer_reductioncard` (
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_reduction_card` int(10) UNSIGNED NOT NULL,
  `valid_until` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `reductions`
--

CREATE TABLE `reductions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `price_rate` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `reduction_cards`
--

CREATE TABLE `reduction_cards` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `price_rate` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `segments`
--

CREATE TABLE `segments` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_train` int(10) UNSIGNED NOT NULL,
  `id_departure_station` int(10) UNSIGNED NOT NULL,
  `departure_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_arrival_station` int(10) UNSIGNED NOT NULL,
  `arrival_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `initial_price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `stations`
--

CREATE TABLE `stations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tickets`
--

CREATE TABLE `tickets` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_departure_segment` int(10) UNSIGNED NOT NULL,
  `id_arrival_segment` int(10) UNSIGNED NOT NULL,
  `id_coach_seat` int(10) UNSIGNED NOT NULL,
  `price` double NOT NULL,
  `state` enum('canceled','pending','confirmed') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ticket_reduction`
--

CREATE TABLE `ticket_reduction` (
  `id_ticket` int(10) UNSIGNED NOT NULL,
  `id_reduction` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `trains`
--

CREATE TABLE `trains` (
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `train_coaches`
--

CREATE TABLE `train_coaches` (
  `id_train` int(10) UNSIGNED NOT NULL,
  `id_coach` int(10) UNSIGNED NOT NULL,
  `coach_num` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`num`);

--
-- Index pour la table `coaches`
--
ALTER TABLE `coaches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_class` (`num_class`);

--
-- Index pour la table `coach_seats`
--
ALTER TABLE `coach_seats`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_coach` (`id_coach`,`seat_num`);

--
-- Index pour la table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `last_name` (`last_name`);

--
-- Index pour la table `customer_reductioncard`
--
ALTER TABLE `customer_reductioncard`
  ADD PRIMARY KEY (`id_customer`,`id_reduction_card`),
  ADD UNIQUE KEY `id_user` (`id_customer`),
  ADD KEY `id_reduction_card` (`id_reduction_card`);

--
-- Index pour la table `reductions`
--
ALTER TABLE `reductions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Index pour la table `reduction_cards`
--
ALTER TABLE `reduction_cards`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Index pour la table `segments`
--
ALTER TABLE `segments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_departure_station` (`id_departure_station`),
  ADD KEY `id_arrival_station` (`id_arrival_station`),
  ADD KEY `id_train` (`id_train`);

--
-- Index pour la table `stations`
--
ALTER TABLE `stations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Index pour la table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_customer`),
  ADD KEY `id_departure_segment` (`id_departure_segment`),
  ADD KEY `id_arrival_segment` (`id_arrival_segment`),
  ADD KEY `id_coach_seat` (`id_coach_seat`);

--
-- Index pour la table `ticket_reduction`
--
ALTER TABLE `ticket_reduction`
  ADD PRIMARY KEY (`id_ticket`,`id_reduction`),
  ADD UNIQUE KEY `id_ticket` (`id_ticket`),
  ADD KEY `id_reduction` (`id_reduction`);

--
-- Index pour la table `trains`
--
ALTER TABLE `trains`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `train_coaches`
--
ALTER TABLE `train_coaches`
  ADD PRIMARY KEY (`id_train`,`id_coach`),
  ADD UNIQUE KEY `id_train` (`id_train`,`coach_num`),
  ADD KEY `id_coach` (`id_coach`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `coaches`
--
ALTER TABLE `coaches`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `coach_seats`
--
ALTER TABLE `coach_seats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `reductions`
--
ALTER TABLE `reductions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `reduction_cards`
--
ALTER TABLE `reduction_cards`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `segments`
--
ALTER TABLE `segments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `stations`
--
ALTER TABLE `stations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `trains`
--
ALTER TABLE `trains`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `coaches`
--
ALTER TABLE `coaches`
  ADD CONSTRAINT `coaches_ibfk_1` FOREIGN KEY (`num_class`) REFERENCES `classes` (`num`);

--
-- Contraintes pour la table `coach_seats`
--
ALTER TABLE `coach_seats`
  ADD CONSTRAINT `coach_seats_ibfk_1` FOREIGN KEY (`id_coach`) REFERENCES `coaches` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `customer_reductioncard`
--
ALTER TABLE `customer_reductioncard`
  ADD CONSTRAINT `customer_reductioncard_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `customer_reductioncard_ibfk_2` FOREIGN KEY (`id_reduction_card`) REFERENCES `reduction_cards` (`id`);

--
-- Contraintes pour la table `segments`
--
ALTER TABLE `segments`
  ADD CONSTRAINT `segments_ibfk_1` FOREIGN KEY (`id_departure_station`) REFERENCES `stations` (`id`),
  ADD CONSTRAINT `segments_ibfk_2` FOREIGN KEY (`id_arrival_station`) REFERENCES `stations` (`id`),
  ADD CONSTRAINT `segments_ibfk_3` FOREIGN KEY (`id_train`) REFERENCES `trains` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `tickets`
--
ALTER TABLE `tickets`
  ADD CONSTRAINT `tickets_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tickets_ibfk_2` FOREIGN KEY (`id_departure_segment`) REFERENCES `segments` (`id`),
  ADD CONSTRAINT `tickets_ibfk_3` FOREIGN KEY (`id_arrival_segment`) REFERENCES `segments` (`id`),
  ADD CONSTRAINT `tickets_ibfk_4` FOREIGN KEY (`id_coach_seat`) REFERENCES `coach_seats` (`id`);

--
-- Contraintes pour la table `ticket_reduction`
--
ALTER TABLE `ticket_reduction`
  ADD CONSTRAINT `ticket_reduction_ibfk_1` FOREIGN KEY (`id_ticket`) REFERENCES `tickets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ticket_reduction_ibfk_2` FOREIGN KEY (`id_reduction`) REFERENCES `reductions` (`id`);

--
-- Contraintes pour la table `train_coaches`
--
ALTER TABLE `train_coaches`
  ADD CONSTRAINT `train_coaches_ibfk_1` FOREIGN KEY (`id_train`) REFERENCES `trains` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `train_coaches_ibfk_2` FOREIGN KEY (`id_coach`) REFERENCES `coaches` (`id`);

--
-- Views
--
CREATE VIEW `nb_seats_by_coach` AS
  SELECT coach_seats.id_coach AS id_coach, COUNT(*) AS nb_seats
  FROM coach_seats
  GROUP BY coach_seats.id_coach;

CREATE VIEW `customers_view` AS
  SELECT customers.id, customers.first_name, customers.last_name, customers.birthdate, reduction_cards.id AS card_id, reduction_cards.name AS card_name, reduction_cards.price_rate, customer_reductioncard.valid_until
  FROM customers
  LEFT JOIN customer_reductioncard ON customers.id = customer_reductioncard.id_customer
  LEFT JOIN reduction_cards ON customer_reductioncard.id_reduction_card = reduction_cards.id
  ORDER BY customers.id;

CREATE VIEW `coaches_view` AS
  SELECT coaches.id AS id, coaches.num_class AS class, COUNT(*) AS capacity
  FROM coaches
  INNER JOIN coach_seats ON coaches.id = coach_seats.id_coach
  GROUP BY coach_seats.id_coach;

CREATE VIEW `coach_seats_view` AS
  SELECT id_coach, situation, COUNT(seat_num) AS nb_seats
  FROM coach_seats
  GROUP BY id_coach, situation;

CREATE VIEW `segments_view` AS
  SELECT segments.*, st1.name AS departure_station, st2.name AS arrival_station
  FROM segments
  INNER JOIN stations st1 ON st1.id = segments.id_departure_station
  INNER JOIN stations st2 ON st2.id = segments.id_arrival_station;

CREATE VIEW `trains_view` AS
  SELECT trains.id, st1.name AS departure_station, st2.name AS arrival_station, st1.id AS id_departure_station, st2.id AS id_arrival_station, s1.departure_time, s2.arrival_time, DATE_FORMAT(s1.departure_time, '%Y-%m-%d') AS departure_date
  FROM trains
  INNER JOIN segments s1 ON trains.id = s1.id_train
  INNER JOIN segments s2  ON trains.id = s2.id_train
  INNER JOIN (
    SELECT id_train, MIN(departure_time) AS min_departure_time, MAX(arrival_time) AS max_arrival_time FROM segments GROUP BY id_train
  )minmaxtbl
  ON trains.id = minmaxtbl.id_train
  INNER JOIN stations st1 ON s1.id_departure_station = st1.id
  INNER JOIN stations st2 ON s2.id_arrival_station = st2.id
  WHERE s1.departure_time = minmaxtbl.min_departure_time AND s2.arrival_time = minmaxtbl.max_arrival_time
  ORDER BY s1.departure_time;

CREATE VIEW `coach_travels_view` AS
  SELECT coaches.id AS id_coach, trains_view.id AS id_train, trains_view.departure_station, trains_view.arrival_station, trains_view.id_departure_station, trains_view.id_arrival_station, trains_view.departure_time, trains_view.arrival_time
  FROM coaches
  LEFT JOIN train_coaches ON coaches.id = train_coaches.id_coach
  LEFT JOIN trains_view ON train_coaches.id_train = trains_view.id;

CREATE VIEW `tickets_customer_view` AS
  SELECT tickets.id AS id_ticket, tickets.id_customer, s1.id_train, tickets.id_coach_seat, tickets.state, st1.name AS departure_station, st2.name AS arrival_station, st1.id AS id_departure_station, st2.id AS id_arrival_station, s1.departure_time, s2.arrival_time, tickets.price
  FROM tickets
  INNER JOIN segments s1 ON s1.id = tickets.id_departure_segment
  INNER JOIN segments s2 ON s2.id = tickets.id_arrival_segment
  INNER JOIN stations st1 ON st1.id = s1.id_departure_station
  INNER JOIN stations st2 ON st2.id = s2.id_arrival_station;

CREATE VIEW `tickets_view` AS
  SELECT
    tickets_customer_view.id_ticket AS id,
    tickets_customer_view.id_customer,
    customers.first_name,
    customers.last_name,
    reduction_cards.name AS card_name,
    reduction_cards.price_rate AS reduction_card_price_rate,
    tickets_customer_view.state,
    tickets_customer_view.departure_station,
    tickets_customer_view.arrival_station,
    tickets_customer_view.id_departure_station,
    tickets_customer_view.id_arrival_station,
    tickets_customer_view.departure_time,
    tickets_customer_view.arrival_time,
    tickets_customer_view.id_train,
    train_coaches.coach_num,
    coaches.num_class AS class,
    classes.price_rate AS class_price_rate,
    coach_seats.seat_num,
    coach_seats.situation,
    reductions.name AS reduction_name,
    reductions.price_rate AS reduction_price_rate,
    tickets_customer_view.price
  FROM tickets_customer_view
  INNER JOIN coach_seats ON tickets_customer_view.id_coach_seat = coach_seats.id
  INNER JOIN train_coaches ON train_coaches.id_train = tickets_customer_view.id_train AND train_coaches.id_coach = coach_seats.id_coach
  INNER JOIN coaches ON coach_seats.id_coach = coaches.id
  INNER JOIN classes ON coaches.num_class = classes.num
  INNER JOIN customers ON customers.id = tickets_customer_view.id_customer
  LEFT JOIN customer_reductioncard ON customer_reductioncard.id_customer = tickets_customer_view.id_customer
  LEFT JOIN reduction_cards ON customer_reductioncard.id_reduction_card = reduction_cards.id
  LEFT JOIN ticket_reduction ON tickets_customer_view.id_ticket = ticket_reduction.id_ticket
  LEFT JOIN reductions ON reductions.id = ticket_reduction.id_reduction;
  
--
-- Procedures
--
DELIMITER $$

CREATE PROCEDURE is_seat_available(IN departure_segment INT, IN arrival_segment INT, IN coach_seat INT, IN ticket_id_to_ignore INT, OUT available BOOLEAN)
BEGIN
  -- we assume departure_segment and arrival_segment are from the same trains

  SELECT id_train INTO @idTrain FROM segments WHERE id = departure_segment;

  SELECT id_train INTO @idTrain2 FROM segments WHERE id = arrival_segment;

  IF @idTrain != @idTrain2 OR @idTrain NOT IN (SELECT train_coaches.id_train FROM train_coaches INNER JOIN coach_seats ON coach_seats.id_coach = train_coaches.id_coach WHERE coach_seats.id = coach_seat) THEN
    SET available = FALSE;

  ELSE

    SELECT COUNT(tickets.id) INTO @nbTickets  FROM tickets
    INNER JOIN segments s1 ON s1.id = tickets.id_departure_segment
    INNER JOIN segments s2 ON s2.id = tickets.id_arrival_segment
    WHERE
    tickets.state != 'canceled'
    AND
    s1.id_train = @idTrain
    AND
      ((s2.arrival_time - (SELECT departure_time FROM segments WHERE segments.id = departure_segment))
      *
      (s1.departure_time - (SELECT arrival_time FROM segments WHERE segments.id = arrival_segment)) < 0)
    AND tickets.id_coach_seat = coach_seat
    AND tickets.id != ticket_id_to_ignore;

    IF @nbTickets = 0
    THEN SET available = TRUE;
    ELSE SET available = FALSE;
    END IF;
  
  END IF;
END$$

CREATE PROCEDURE get_available_seats(IN departure_segment INT, IN arrival_segment INT)
BEGIN
  DROP TABLE IF EXISTS available_seats;
  CREATE TEMPORARY TABLE available_seats AS(
  SELECT coach_seats.id AS id_coach_seat, coaches.num_class, coach_seats.situation FROM coach_seats
  INNER JOIN train_coaches ON train_coaches.id_coach = coach_seats.id_coach
  INNER JOIN coaches ON coaches.id = coach_seats.id_coach
  WHERE train_coaches.id_train = (SELECT id_train FROM segments WHERE id = departure_segment)
  AND train_coaches.id_train = (SELECT id_train FROM segments WHERE id = arrival_segment)
  AND coach_seats.id NOT IN 
  (
    SELECT coach_seats.id AS id_coach_seat FROM tickets
    INNER JOIN segments s1 ON s1.id = tickets.id_departure_segment
    INNER JOIN segments s2 ON s2.id = tickets.id_arrival_segment
    INNER JOIN coach_seats ON coach_seats.id = tickets.id_coach_seat
    WHERE
    tickets.state != 'canceled'
    AND
    s1.id_train = (SELECT id_train FROM segments WHERE segments.id = departure_segment)
    AND
    ((s2.arrival_time - (SELECT departure_time FROM segments WHERE segments.id = departure_segment))
    *
    (s1.departure_time - (SELECT arrival_time FROM segments WHERE segments.id = arrival_segment)) < 0)
  ));
END$$

CREATE PROCEDURE get_ticket_price(IN departure_segment INT, IN arrival_segment INT, IN class SMALLINT, IN customer INT, IN id_reduction INT, OUT price DOUBLE)
BEGIN
  DECLARE initial_price DOUBLE;
  DECLARE class_price_rate DOUBLE;
  DECLARE reduction_price_rate DOUBLE;
  DECLARE reduction_card_price_rate DOUBLE;
  DECLARE dep_time TIMESTAMP;

  SELECT departure_time INTO dep_time FROM segments WHERE id = departure_segment;

  SELECT SUM(segments.initial_price) INTO initial_price
  FROM segments
  INNER JOIN segments dep_seg ON dep_seg.id_train = segments.id_train
  INNER JOIN segments arr_seg ON arr_seg.id_train = segments.id_train
  WHERE dep_seg.id = departure_segment
  AND arr_seg.id = arrival_segment
  AND segments.departure_time >= dep_seg.departure_time
  AND segments.arrival_time <= arr_seg.arrival_time;

  SELECT price_rate INTO class_price_rate
  FROM classes
  WHERE num = class
  LIMIT 1;

  SELECT price_rate INTO reduction_price_rate
  FROM reductions
  WHERE id = id_reduction
  LIMIT 1;

  SELECT price_rate INTO reduction_card_price_rate
  FROM reduction_cards
  INNER JOIN customer_reductioncard ON customer_reductioncard.id_reduction_card = reduction_cards.id
  WHERE customer_reductioncard.id_customer = customer AND customer_reductioncard.valid_until>=dep_time
  LIMIT 1;
  
  IF reduction_price_rate      IS NULL THEN SET reduction_price_rate = 1;      END IF;
  IF reduction_card_price_rate IS NULL THEN SET reduction_card_price_rate = 1; END IF;
  
  SET price = ROUND(initial_price*class_price_rate*reduction_price_rate*reduction_card_price_rate*100)/100;
END$$

CREATE PROCEDURE create_ticket(IN departure_segment INT, IN arrival_segment INT, IN class INT, IN sit ENUM('window','aisle'), IN customer INT, IN reduction INT, OUT insert_id INT)
BEGIN
  CALL get_available_seats(departure_segment, arrival_segment);
  SET @coach_seat_to_book = NULL;
  IF sit IS NOT NULL THEN
    SELECT id_coach_seat INTO @coach_seat_to_book FROM available_seats WHERE num_class=class AND situation=sit ORDER BY id_coach_seat LIMIT 1;
  ELSE
    SELECT id_coach_seat INTO @coach_seat_to_book FROM available_seats WHERE num_class=class ORDER BY id_coach_seat LIMIT 1;
  END IF;

  IF @coach_seat_to_book IS NULL THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='no more available seats';
  ELSE

    -- Check reduction
    IF (SELECT id FROM reductions WHERE id = reduction LIMIT 1) IS NULL
    THEN
      SET reduction = NULL;
    END IF;

    CALL get_ticket_price(departure_segment, arrival_segment, class, customer, reduction, @price);
    IF @price IS NOT NULL THEN
      INSERT INTO `tickets` (`id`, `id_customer`, `id_departure_segment`, `id_arrival_segment`, `id_coach_seat`, `price`, `state`)
      VALUES (NULL, customer, departure_segment, arrival_segment, @coach_seat_to_book, @price, 'pending');
      
      SELECT id INTO insert_id FROM tickets WHERE id_customer=customer AND id_coach_seat=@coach_seat_to_book AND id_departure_segment=departure_segment AND id_arrival_segment=arrival_segment LIMIT 1;

      IF reduction IS NOT NULL
      THEN
        INSERT INTO `ticket_reduction` (`id_ticket`, `id_reduction`) VALUES (insert_id, reduction);
      END IF;
    END IF;
  END IF;
END$$

DELIMITER ;

--
-- Triggers for checks
--
DELIMITER $$

CREATE PROCEDURE price_rate_is_reduction(IN price_rate REAL, OUT ok BOOLEAN)
BEGIN
  IF price_rate >= 1 THEN
    SET ok = FALSE;
  ELSE
    SET ok = TRUE;
  END IF;
END$$

CREATE TRIGGER `check_reductions_insert_trigger`
BEFORE INSERT ON `reductions`
FOR EACH ROW
BEGIN
  CALL price_rate_is_reduction(NEW.price_rate, @ok);
  IF NOT @ok THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='invalid price rate';
  END IF;
END$$

CREATE TRIGGER `check_reductions_update_trigger`
BEFORE UPDATE ON `reductions`
FOR EACH ROW
BEGIN
  CALL price_rate_is_reduction(NEW.price_rate, @ok);
  IF NOT @ok THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='invalid price rate';
  END IF;
END$$

CREATE TRIGGER `check_reduction_cards_insert_trigger`
BEFORE INSERT ON `reduction_cards`
FOR EACH ROW
BEGIN
  CALL price_rate_is_reduction(NEW.price_rate, @ok);
  IF NOT @ok THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='invalid price rate';
  END IF;
END$$

CREATE TRIGGER `check_reduction_cards_update_trigger`
BEFORE UPDATE ON `reduction_cards`
FOR EACH ROW
BEGIN
  CALL price_rate_is_reduction(NEW.price_rate, @ok);
  IF NOT @ok THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='invalid price rate';
  END IF;
END$$

CREATE PROCEDURE departure_is_before_arrival(IN departure TIMESTAMP, IN arrival TIMESTAMP, OUT ok BOOLEAN)
BEGIN
  IF departure >= arrival THEN
    SET ok = FALSE;
  ELSE
    SET ok = TRUE;
  END IF;
END$$

CREATE TRIGGER `check_segments_insert_trigger`
BEFORE INSERT ON `segments`
FOR EACH ROW
BEGIN
  CALL departure_is_before_arrival(NEW.departure_time, NEW.arrival_time, @ok);
  IF NOT @ok THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='invalid departure - arrival';
  END IF;
END$$

CREATE TRIGGER `check_segments_update_trigger`
BEFORE UPDATE ON `segments`
FOR EACH ROW
BEGIN
  CALL departure_is_before_arrival(NEW.departure_time, NEW.arrival_time, @ok);
  IF NOT @ok THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='invalid departure - arrival';
  END IF;
END$$

CREATE PROCEDURE is_ticket_valid(IN departure_segment INT, IN arrival_segment INT, IN coach_seat INT, IN old_ticket_id INT, OUT ok BOOLEAN)
BEGIN
  -- check departure and arrival segments correspond to the same trains and departure is before arrival
  SELECT id_train, departure_time INTO @dep_seg_train, @depTime1 FROM segments WHERE id=departure_segment;
  SELECT id_train, departure_time INTO @arr_seg_train, @depTime2 FROM segments WHERE id=arrival_segment;
  IF @dep_seg_train = @arr_seg_train AND @depTime1 <= @depTime2 THEN
    -- check coach_seat availability
    IF coach_seat IS NOT NULL
    THEN CALL is_seat_available(departure_segment, arrival_segment, coach_seat, old_ticket_id, ok);
    ELSE SET ok = TRUE;
    END IF;
  ELSE
    SET ok = FALSE;
  END IF;
END$$

CREATE PROCEDURE is_ticket_new_state_valid(IN old_state ENUM('canceled','pending','confirmed'), IN new_state ENUM('canceled','pending','confirmed'), OUT ok BOOLEAN)
BEGIN
    SET ok = NOT ((old_state='canceled' AND new_state!='canceled') OR (old_state='confirmed' AND new_state='pending'));
END$$

CREATE TRIGGER `check_tickets_insert_trigger`
BEFORE INSERT ON `tickets`
FOR EACH ROW
BEGIN
  CALL is_ticket_valid(NEW.id_departure_segment, NEW.id_arrival_segment, NEW.id_coach_seat, NULL, @ok);
  IF NOT @ok THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='invalid ticket';
  END IF;
END$$

CREATE TRIGGER `check_tickets_update_trigger`
BEFORE UPDATE ON `tickets`
FOR EACH ROW
BEGIN
  CALL is_ticket_valid(NEW.id_departure_segment, NEW.id_arrival_segment, NEW.id_coach_seat, OLD.id, @ok);
  IF NOT @ok THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='invalid ticket';
  END IF;

  CALL is_ticket_new_state_valid(OLD.state, NEW.state, @ok2);
  IF NOT @ok2 THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='illegal ticket state transition';
  END IF;
END$$

DELIMITER ;


--
-- User creation
--
CREATE USER 'train_ticket_booking_manager'@'%' IDENTIFIED BY 'train_ticket_booking_manager_password';

--
-- Privileges
--
GRANT SELECT ON `trains_BE_2016`.* TO 'train_ticket_booking_manager'@'%';
GRANT SELECT, INSERT, UPDATE, DELETE ON `trains_BE_2016`.`ticket_reduction` TO 'train_ticket_booking_manager'@'%';
GRANT SELECT, INSERT, UPDATE, DELETE ON `trains_BE_2016`.`customer_reductioncard` TO 'train_ticket_booking_manager'@'%';
GRANT SELECT, INSERT, UPDATE, DELETE ON `trains_BE_2016`.`customers` TO 'train_ticket_booking_manager'@'%';
GRANT SELECT, UPDATE (`state`), DELETE ON `trains_BE_2016`.`tickets` TO 'train_ticket_booking_manager'@'%';

GRANT EXECUTE ON PROCEDURE `trains_BE_2016`.`get_available_seats` TO 'train_ticket_booking_manager'@'%';
GRANT EXECUTE ON PROCEDURE `trains_BE_2016`.`get_ticket_price` TO 'train_ticket_booking_manager'@'%';
GRANT EXECUTE ON PROCEDURE `trains_BE_2016`.`create_ticket` TO 'train_ticket_booking_manager'@'%';
GRANT EXECUTE ON PROCEDURE `trains_BE_2016`.`is_seat_available` TO 'train_ticket_booking_manager'@'%';
GRANT EXECUTE ON PROCEDURE `trains_BE_2016`.`is_ticket_valid` TO 'train_ticket_booking_manager'@'%';
GRANT EXECUTE ON PROCEDURE `trains_BE_2016`.`is_ticket_new_state_valid` TO 'train_ticket_booking_manager'@'%';

--
-- Contenu de la table `classes`
--

INSERT INTO `classes` (`num`, `price_rate`) VALUES
(1, 1.1),
(2, 1);

--
-- Contenu de la table `coaches`
--

INSERT INTO `coaches` (`id`, `num_class`) VALUES
(1, 1),
(2, 1),
(3, 1),
(9, 1),
(10, 1),
(11, 1),
(17, 1),
(18, 1),
(19, 1),
(25, 1),
(26, 1),
(27, 1),
(4, 2),
(5, 2),
(6, 2),
(7, 2),
(8, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(16, 2),
(20, 2),
(21, 2),
(22, 2),
(23, 2),
(24, 2),
(28, 2),
(29, 2),
(30, 2),
(31, 2),
(32, 2);

--
-- Contenu de la table `coach_seats`
--

INSERT INTO `coach_seats` (`id`, `id_coach`, `seat_num`, `situation`) VALUES
(1, 1, 1, 'window'),
(2, 2, 1, 'window'),
(3, 3, 1, 'window'),
(4, 4, 1, 'window'),
(5, 5, 1, 'window'),
(6, 6, 1, 'window'),
(7, 7, 1, 'window'),
(8, 8, 1, 'window'),
(9, 9, 1, 'window'),
(10, 10, 1, 'window'),
(11, 11, 1, 'window'),
(12, 12, 1, 'window'),
(13, 13, 1, 'window'),
(14, 14, 1, 'window'),
(15, 15, 1, 'window'),
(16, 16, 1, 'window'),
(17, 17, 1, 'window'),
(18, 18, 1, 'window'),
(19, 19, 1, 'window'),
(20, 20, 1, 'window'),
(21, 21, 1, 'window'),
(22, 22, 1, 'window'),
(23, 23, 1, 'window'),
(24, 24, 1, 'window'),
(25, 25, 1, 'window'),
(26, 26, 1, 'window'),
(27, 27, 1, 'window'),
(28, 28, 1, 'window'),
(29, 29, 1, 'window'),
(30, 30, 1, 'window'),
(31, 31, 1, 'window'),
(32, 32, 1, 'window'),
(33, 1, 2, 'window'),
(34, 2, 2, 'window'),
(35, 3, 2, 'window'),
(36, 4, 2, 'window'),
(37, 5, 2, 'window'),
(38, 6, 2, 'window'),
(39, 7, 2, 'window'),
(40, 8, 2, 'window'),
(41, 9, 2, 'window'),
(42, 10, 2, 'window'),
(43, 11, 2, 'window'),
(44, 12, 2, 'window'),
(45, 13, 2, 'window'),
(46, 14, 2, 'window'),
(47, 15, 2, 'window'),
(48, 16, 2, 'window'),
(49, 17, 2, 'window'),
(50, 18, 2, 'window'),
(51, 19, 2, 'window'),
(52, 20, 2, 'window'),
(53, 21, 2, 'window'),
(54, 22, 2, 'window'),
(55, 23, 2, 'window'),
(56, 24, 2, 'window'),
(57, 25, 2, 'window'),
(58, 26, 2, 'window'),
(59, 27, 2, 'window'),
(60, 28, 2, 'window'),
(61, 29, 2, 'window'),
(62, 30, 2, 'window'),
(63, 31, 2, 'window'),
(64, 32, 2, 'window'),
(65, 1, 3, 'aisle'),
(66, 2, 3, 'aisle'),
(67, 3, 3, 'aisle'),
(68, 4, 3, 'aisle'),
(69, 5, 3, 'aisle'),
(70, 6, 3, 'aisle'),
(71, 7, 3, 'aisle'),
(72, 8, 3, 'aisle'),
(73, 9, 3, 'aisle'),
(74, 10, 3, 'aisle'),
(75, 11, 3, 'aisle'),
(76, 12, 3, 'aisle'),
(77, 13, 3, 'aisle'),
(78, 14, 3, 'aisle'),
(79, 15, 3, 'aisle'),
(80, 16, 3, 'aisle'),
(81, 17, 3, 'aisle'),
(82, 18, 3, 'aisle'),
(83, 19, 3, 'aisle'),
(84, 20, 3, 'aisle'),
(85, 21, 3, 'aisle'),
(86, 22, 3, 'aisle'),
(87, 23, 3, 'aisle'),
(88, 24, 3, 'aisle'),
(89, 25, 3, 'aisle'),
(90, 26, 3, 'aisle'),
(91, 27, 3, 'aisle'),
(92, 28, 3, 'aisle'),
(93, 29, 3, 'aisle'),
(94, 30, 3, 'aisle'),
(95, 31, 3, 'aisle'),
(96, 32, 3, 'aisle'),
(97, 1, 4, 'aisle'),
(98, 2, 4, 'aisle'),
(99, 3, 4, 'aisle'),
(100, 4, 4, 'aisle'),
(101, 5, 4, 'aisle'),
(102, 6, 4, 'aisle'),
(103, 7, 4, 'aisle'),
(104, 8, 4, 'aisle'),
(105, 9, 4, 'aisle'),
(106, 10, 4, 'aisle'),
(107, 11, 4, 'aisle'),
(108, 12, 4, 'aisle'),
(109, 13, 4, 'aisle'),
(110, 14, 4, 'aisle'),
(111, 15, 4, 'aisle'),
(112, 16, 4, 'aisle'),
(113, 17, 4, 'aisle'),
(114, 18, 4, 'aisle'),
(115, 19, 4, 'aisle'),
(116, 20, 4, 'aisle'),
(117, 21, 4, 'aisle'),
(118, 22, 4, 'aisle'),
(119, 23, 4, 'aisle'),
(120, 24, 4, 'aisle'),
(121, 25, 4, 'aisle'),
(122, 26, 4, 'aisle'),
(123, 27, 4, 'aisle'),
(124, 28, 4, 'aisle'),
(125, 29, 4, 'aisle'),
(126, 30, 4, 'aisle'),
(127, 31, 4, 'aisle'),
(128, 32, 4, 'aisle'),
(129, 1, 5, 'aisle'),
(130, 2, 5, 'aisle'),
(131, 3, 5, 'aisle'),
(132, 4, 5, 'aisle'),
(133, 5, 5, 'aisle'),
(134, 6, 5, 'aisle'),
(135, 7, 5, 'aisle'),
(136, 8, 5, 'aisle'),
(137, 9, 5, 'aisle'),
(138, 10, 5, 'aisle'),
(139, 11, 5, 'aisle'),
(140, 12, 5, 'aisle'),
(141, 13, 5, 'aisle'),
(142, 14, 5, 'aisle'),
(143, 15, 5, 'aisle'),
(144, 16, 5, 'aisle'),
(145, 17, 5, 'aisle'),
(146, 18, 5, 'aisle'),
(147, 19, 5, 'aisle'),
(148, 20, 5, 'aisle'),
(149, 21, 5, 'aisle'),
(150, 22, 5, 'aisle'),
(151, 23, 5, 'aisle'),
(152, 24, 5, 'aisle'),
(153, 25, 5, 'aisle'),
(154, 26, 5, 'aisle'),
(155, 27, 5, 'aisle'),
(156, 28, 5, 'aisle'),
(157, 29, 5, 'aisle'),
(158, 30, 5, 'aisle'),
(159, 31, 5, 'aisle'),
(160, 32, 5, 'aisle');

--
-- Contenu de la table `customers`
--

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `birthdate`) VALUES
(1, 'Thomas', 'Pucci', '1993-07-23'),
(2, 'Yann', 'Vaginay', '1993-11-15');

--
-- Contenu de la table `reduction_cards`
--

INSERT INTO `reduction_cards` (`id`, `name`, `price_rate`) VALUES
(1, 'Carte Jeune', 0.9),
(2, 'Carte Voyageur', 0.9),
(3, 'Carte Grand Voyageur', 0.8);

--
-- Contenu de la table `customer_reductioncard`
--

INSERT INTO `customer_reductioncard` (`id_customer`, `id_reduction_card`, `valid_until`) VALUES
(1, 1, '2016-12-30'),
(2, 3, '2016-12-31');

--
-- Contenu de la table `reductions`
--

INSERT INTO `reductions` (`id`, `name`, `price_rate`) VALUES
(1, 'Offre Go', 0.9),
(2, 'Weekend Lumières', 0.8),
(3, 'Offre Novembre', 0.9),
(4, 'Offre Décembre', 0.9),
(5, 'Offre ECL', 0.7);

--
-- Contenu de la table `stations`
--

INSERT INTO `stations` (`id`, `name`) VALUES
(10, 'Bordeaux'),
(7, 'Lille'),
(4, 'Lyon'),
(2, 'Marseille'),
(3, 'Montpellier'),
(8, 'Nantes'),
(1, 'Nice'),
(6, 'Paris'),
(5, 'Strasbourg'),
(9, 'Toulouse');

--
-- Contenu de la table `trains`
--

INSERT INTO `trains` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18),
(19),
(20);

--
-- Contenu de la table `segments`
--

INSERT INTO `segments` (`id`, `id_train`, `id_departure_station`, `departure_time`, `id_arrival_station`, `arrival_time`, `initial_price`) VALUES
(1, 1, 1, '2016-12-16 08:30:00', 2, '2016-12-16 10:30:00', 10),
(2, 1, 2, '2016-12-16 10:30:00', 3, '2016-12-16 12:30:00', 12),
(3, 2, 1, '2016-12-16 10:00:00', 2, '2016-12-16 13:00:00', 10),
(4, 2, 2, '2016-12-16 13:00:00', 4, '2016-12-16 16:00:00', 13),
(5, 2, 4, '2016-12-16 16:00:00', 6, '2016-12-16 19:00:00', 15),
(6, 3, 8, '2016-12-24 08:30:00', 6, '2016-12-24 10:35:00', 10),
(7, 3, 6, '2016-12-24 10:35:00', 5, '2016-12-24 12:30:00', 12),
(8, 4, 9, '2016-12-30 11:09:00', 10, '2016-12-30 13:00:00', 10),
(9, 4, 10, '2016-12-30 13:00:00', 8, '2016-12-30 16:00:00', 13),
(10, 4, 8, '2016-12-30 16:00:00', 7, '2016-12-30 19:23:00', 15);

--
-- Contenu de la table `train_coaches`
--

INSERT INTO `train_coaches` (`id_train`, `id_coach`, `coach_num`) VALUES
(1, 1, 1),
(1, 4, 2),
(2, 2, 1),
(2, 5, 2),
(3, 3, 1),
(3, 2, 2),
(3, 5, 3),
(4, 4, 1),
(4, 5, 2),
(4, 2, 3),
(5, 5, 1),
(6, 6, 1),
(7, 7, 1),
(8, 8, 1),
(9, 9, 1),
(10, 10, 1),
(11, 11, 1),
(12, 12, 1),
(13, 13, 1),
(14, 14, 1),
(15, 15, 1),
(16, 16, 1),
(17, 17, 1),
(18, 18, 1),
(19, 19, 1),
(20, 20, 1),
(20, 21, 2),
(20, 22, 3),
(20, 23, 4),
(20, 24, 5),
(20, 25, 6),
(20, 26, 7),
(20, 27, 8),
(20, 28, 9),
(20, 29, 10),
(20, 30, 11),
(20, 31, 12),
(20, 32, 13);

--
-- Contenu de la table `tickets`
--

INSERT INTO `tickets` (`id`, `id_customer`, `id_departure_segment`, `id_arrival_segment`, `id_coach_seat`, `price`, `state`) VALUES
(26, 1, 1, 1, 1, 9.9, 'canceled'),
(27, 1, 1, 1, 65, 11, 'canceled');
