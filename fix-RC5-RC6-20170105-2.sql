DROP PROCEDURE get_ticket_price;
DELIMITER $$
CREATE PROCEDURE get_ticket_price(IN departure_segment INT, IN arrival_segment INT, IN class SMALLINT, IN customer INT, IN id_reduction INT, OUT price DOUBLE)
BEGIN
  DECLARE initial_price DOUBLE;
  DECLARE class_price_rate DOUBLE;
  DECLARE reduction_price_rate DOUBLE;
  DECLARE reduction_card_price_rate DOUBLE;
  DECLARE dep_time TIMESTAMP;

  SELECT departure_time INTO dep_time FROM segments WHERE id = departure_segment;

  SELECT SUM(segments.initial_price) INTO initial_price
  FROM segments
  INNER JOIN segments dep_seg ON dep_seg.id_train = segments.id_train
  INNER JOIN segments arr_seg ON arr_seg.id_train = segments.id_train
  WHERE dep_seg.id = departure_segment
  AND arr_seg.id = arrival_segment
  AND segments.departure_time >= dep_seg.departure_time
  AND segments.arrival_time <= arr_seg.arrival_time;

  SELECT price_rate INTO class_price_rate
  FROM classes
  WHERE num = class
  LIMIT 1;

  SELECT price_rate INTO reduction_price_rate
  FROM reductions
  WHERE id = id_reduction
  LIMIT 1;

  SELECT price_rate INTO reduction_card_price_rate
  FROM reduction_cards
  INNER JOIN customer_reductioncard ON customer_reductioncard.id_reduction_card = reduction_cards.id
  WHERE customer_reductioncard.id_customer = customer AND customer_reductioncard.valid_until>=dep_time
  LIMIT 1;
  
  IF reduction_price_rate      IS NULL THEN SET reduction_price_rate = 1;      END IF;
  IF reduction_card_price_rate IS NULL THEN SET reduction_card_price_rate = 1; END IF;
  
  SET price = ROUND(initial_price*class_price_rate*reduction_price_rate*reduction_card_price_rate*100)/100;
END$$
DELIMITER ;