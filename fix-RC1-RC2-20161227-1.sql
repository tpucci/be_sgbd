-- Fix

DROP VIEW IF EXISTS coach_seats_by_segment;

-- Fix

DROP PROCEDURE IF EXISTS is_seat_available;
DELIMITER $$

CREATE PROCEDURE is_seat_available(IN departure_segment INT, IN arrival_segment INT, IN coach_seat INT, IN ticket_id_to_ignore INT, OUT available BOOLEAN)
BEGIN
  -- we assume departure_segment and arrival_segment are from the same trains

  SELECT id_train INTO @idTrain FROM segments WHERE id = departure_segment;

  SELECT id_train INTO @idTrain2 FROM segments WHERE id = arrival_segment;

  IF @idTrain != @idTrain2 OR @idTrain NOT IN (SELECT train_coaches.id_train FROM train_coaches INNER JOIN coach_seats ON coach_seats.id_coach = train_coaches.id_coach WHERE coach_seats.id = coach_seat) THEN
    SET available = FALSE;

  ELSE

    SELECT COUNT(tickets.id) INTO @nbTickets  FROM tickets
    INNER JOIN segments s1 ON s1.id = tickets.id_departure_segment
    INNER JOIN segments s2 ON s2.id = tickets.id_arrival_segment
    WHERE s1.id_train = @idTrain
    AND
      ((s2.arrival_time - (SELECT departure_time FROM segments WHERE segments.id = departure_segment))
      *
      (s1.departure_time - (SELECT arrival_time FROM segments WHERE segments.id = arrival_segment)) < 0)
    AND tickets.id_coach_seat = coach_seat
    AND tickets.id != ticket_id_to_ignore;

    IF @nbTickets = 0
    THEN SET available = TRUE;
    ELSE SET available = FALSE;
    END IF;
  
  END IF;
END$$

DELIMITER ;

GRANT EXECUTE ON PROCEDURE `trains_BE_2016`.`is_seat_available` TO 'train_ticket_booking_manager'@'%';

-- Fix

DROP VIEW IF EXISTS tickets_view;
CREATE VIEW `tickets_view` AS
  SELECT
    tickets_customer_view.id_ticket AS id,
    tickets_customer_view.id_customer,
    customers.first_name,
    customers.last_name,
    reduction_cards.name AS card_name,
    reduction_cards.price_rate AS reduction_card_price_rate,
    tickets_customer_view.state,
    tickets_customer_view.departure_station,
    tickets_customer_view.arrival_station,
    tickets_customer_view.id_departure_station,
    tickets_customer_view.id_arrival_station,
    tickets_customer_view.departure_time,
    tickets_customer_view.arrival_time,
    tickets_customer_view.id_train,
    train_coaches.coach_num,
    coaches.num_class AS class,
    classes.price_rate AS class_price_rate,
    coach_seats.seat_num,
    coach_seats.situation,
    reductions.name AS reduction_name,
    reductions.price_rate AS reduction_price_rate,
    tickets_customer_view.price
  FROM tickets_customer_view
  INNER JOIN coach_seats ON tickets_customer_view.id_coach_seat = coach_seats.id
  INNER JOIN train_coaches ON train_coaches.id_train = tickets_customer_view.id_train AND train_coaches.id_coach = coach_seats.id_coach
  INNER JOIN coaches ON coach_seats.id_coach = coaches.id
  INNER JOIN classes ON coaches.num_class = classes.num
  INNER JOIN customers ON customers.id = tickets_customer_view.id_customer
  LEFT JOIN customer_reductioncard ON customer_reductioncard.id_customer = tickets_customer_view.id_customer
  LEFT JOIN reduction_cards ON customer_reductioncard.id_reduction_card = reduction_cards.id
  LEFT JOIN ticket_reduction ON tickets_customer_view.id_ticket = ticket_reduction.id_ticket
  LEFT JOIN reductions ON reductions.id = ticket_reduction.id_reduction;

-- Feature

REVOKE ALL PRIVILEGES ON `trains_BE_2016`.`tickets` FROM 'train_ticket_booking_manager'@'%';
GRANT SELECT, UPDATE (`state`), DELETE ON `trains_BE_2016`.`tickets` TO 'train_ticket_booking_manager'@'%';

DROP PROCEDURE IF EXISTS is_ticket_new_state_valid;
DELIMITER $$
CREATE PROCEDURE is_ticket_new_state_valid(IN old_state ENUM('canceled','pending','confirmed'), IN new_state ENUM('canceled','pending','confirmed'), OUT ok BOOLEAN)
BEGIN
    SET ok = NOT ((old_state='canceled' AND new_state!='canceled') OR (old_state='confirmed' AND new_state='pending'));
END$$
DELIMITER ;

GRANT EXECUTE ON PROCEDURE `trains_BE_2016`.`is_ticket_new_state_valid` TO 'train_ticket_booking_manager'@'%';

DROP TRIGGER IF EXISTS check_tickets_update_trigger;
DELIMITER $$
CREATE TRIGGER `check_tickets_update_trigger`
BEFORE UPDATE ON `tickets`
FOR EACH ROW
BEGIN
  CALL is_ticket_valid(NEW.id_departure_segment, NEW.id_arrival_segment, NEW.id_coach_seat, OLD.id, @ok);
  IF NOT @ok THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='invalid ticket';
  END IF;

  CALL is_ticket_new_state_valid(OLD.state, NEW.state, @ok2);
  IF NOT @ok2 THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='illegal ticket state transition';
  END IF;
END$$
DELIMITER ;
