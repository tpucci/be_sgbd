-- Fix

DROP VIEW IF EXISTS trains_view;
CREATE VIEW `trains_view` AS
  SELECT trains.id, st1.name AS departure_station, st2.name AS arrival_station, st1.id AS id_departure_station, st2.id AS id_arrival_station, s1.departure_time, s2.arrival_time, DATE_FORMAT(s1.departure_time, '%Y-%m-%d') AS departure_date
  FROM trains
  INNER JOIN segments s1 ON trains.id = s1.id_train
  INNER JOIN segments s2  ON trains.id = s2.id_train
  INNER JOIN (
    SELECT id_train, MIN(departure_time) AS min_departure_time, MAX(arrival_time) AS max_arrival_time FROM segments GROUP BY id_train
  )minmaxtbl
  ON trains.id = minmaxtbl.id_train
  INNER JOIN stations st1 ON s1.id_departure_station = st1.id
  INNER JOIN stations st2 ON s2.id_arrival_station = st2.id
  WHERE s1.departure_time = minmaxtbl.min_departure_time AND s2.arrival_time = minmaxtbl.max_arrival_time
  ORDER BY s1.departure_time;

-- Fix

DROP PROCEDURE IF EXISTS create_ticket;
DELIMITER $$
CREATE PROCEDURE create_ticket(IN departure_segment INT, IN arrival_segment INT, IN class INT, IN sit ENUM('window','aisle'), IN customer INT, IN reduction INT, OUT insert_id INT)
BEGIN
  CALL get_available_seats(departure_segment, arrival_segment);
  SET @coach_seat_to_book = NULL;
  IF sit IS NOT NULL THEN
    SELECT id_coach_seat INTO @coach_seat_to_book FROM available_seats WHERE num_class=class AND situation=sit ORDER BY id_coach_seat LIMIT 1;
  ELSE
    SELECT id_coach_seat INTO @coach_seat_to_book FROM available_seats WHERE num_class=class ORDER BY id_coach_seat LIMIT 1;
  END IF;

  IF @coach_seat_to_book IS NULL THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='no more available seats';
  ELSE

    -- Check reduction
    IF (SELECT id FROM reductions WHERE id = reduction LIMIT 1) IS NULL
    THEN
      SET reduction = NULL;
    END IF;

    CALL get_ticket_price(departure_segment, arrival_segment, class, customer, reduction, @price);
    IF @price IS NOT NULL THEN
      INSERT INTO `tickets` (`id`, `id_customer`, `id_departure_segment`, `id_arrival_segment`, `id_coach_seat`, `price`, `state`)
      VALUES (NULL, customer, departure_segment, arrival_segment, @coach_seat_to_book, @price, 'pending');
      
      SELECT id INTO insert_id FROM tickets WHERE id_customer=customer AND id_coach_seat=@coach_seat_to_book AND id_departure_segment=departure_segment AND id_arrival_segment=arrival_segment LIMIT 1;

      IF reduction IS NOT NULL
      THEN
        INSERT INTO `ticket_reduction` (`id_ticket`, `id_reduction`) VALUES (insert_id, reduction);
      END IF;
    END IF;
  END IF;
END$$
DELIMITER ;

GRANT EXECUTE ON PROCEDURE `trains_BE_2016`.`create_ticket` TO 'train_ticket_booking_manager'@'%';
