var router = require('express').Router();
var pool = require('./mysqlpool');
var fs = require('fs');

module.exports = router;

var pool = pool();
var apiQuery = function(query, args, callback) {
    if (!args) args = [];
    pool.getConnection(function(err, connection) {
        if(err) callback({'err': err});
        else {
            var cq = connection.query(query, args, function (err, res_sql, fields) {
                if(err) callback({'err': err});
                else callback(res_sql);

                connection.release();
            });
            console.log("SQL Query: " + cq.sql);
        }
    });
};

var sendQueryResult = function(res, query, args, mapper) {
    apiQuery(query, args, function(r) { res.json((!r.err && mapper) ? mapper(r) : r); });
}

//
// GET /api/reductions
// Returns reduction list
//
router.get('/reductions', function (req, res) {
    sendQueryResult(res, 'SELECT * FROM reductions ORDER BY id');
});

//
// GET /api/coaches
// Returns coach list
//
router.get('/coaches', function (req, res) {
    sendQueryResult(res, 'SELECT * FROM coaches_view ORDER BY id');
});

//
// GET /api/coach/{id}
// Returns coach information
//
// id: id of the coach [required]
//
router.get('/coach/:id', function (req, res) {
    var result = {};
    apiQuery('SELECT * FROM coaches_view WHERE id = ?', [req.params.id], function(r) {
        result.coach = r.length ? r[0] : {};
        apiQuery('SELECT situation, nb_seats FROM coach_seats_view WHERE id_coach = ?', [req.params.id], function(r) {
            result.seats = r;
            apiQuery('SELECT id_train, departure_station, arrival_station, id_departure_station, id_arrival_station, departure_time, arrival_time FROM coach_travels_view WHERE id_coach = ?', [req.params.id], function(r) {
                result.travels = r;
                res.json(result);
            });
        });
    });
});

//
// GET /api/c
// Returns customer list for autocompletion
//
router.get('/c', function (req, res) {
    sendQueryResult(res, 'SELECT id, first_name, last_name FROM customers_view ORDER BY id');
});

//
// GET /api/r
// Returns reduction list for autocompletion
//
router.get('/r', function (req, res) {
    sendQueryResult(res, 'SELECT id, name FROM reductions ORDER BY name');
});

//
// GET /api/rc
// Returns reduction card list for autocompletion
//
router.get('/rc', function (req, res) {
    sendQueryResult(res, 'SELECT id, name FROM reduction_cards ORDER BY name');
});

//
// GET /api/customers
// Returns customer list
//
router.get('/customers', function (req, res) {
    sendQueryResult(res, 'SELECT id, first_name, last_name, birthdate, card_name, price_rate FROM customers_view ORDER BY id');
});

//
// GET /api/customer/{id}
// Returns customer information
//
// id: id of the customer [required]
//
router.get('/customer/:id', function (req, res) {
    var result = {};
    apiQuery('SELECT * FROM customers_view WHERE id = ?', [req.params.id], function(r) {
        result.customer = r.length ? r[0] : {};
        apiQuery('SELECT id_ticket, departure_station, arrival_station, departure_time, arrival_time FROM tickets_customer_view WHERE id_customer = ?', [req.params.id], function(r) {
            result.tickets = r;
            res.json(result);
        });
    });
});

//
// GET /api/tickets
// Returns ticket list
//
router.get('/tickets', function (req, res) {
    sendQueryResult(res, 'SELECT * FROM tickets_view ORDER BY id');
});

//
// POST /api/customer
// Adds a customer. The id of the new customer is returned.
//
// Post parameters:
//   first_name: first name of the new customer
//   last_name: last name of the new customer
//   birthdate: birthdate of the new customer
//
router.post('/customer', function (req, res) {
    apiQuery(
        'INSERT INTO customers (id, first_name, last_name, birthdate) VALUES (NULL, ?, ?, ?)',
        [
            req.body.first_name,
            req.body.last_name,
            req.body.birthdate
        ],
        function(customerInsertion) {
            if (customerInsertion.err)
                res.json({'err': customerInsertion.err});

            else {
                customerInsertId = customerInsertion.insertId;
                if (req.body.id_reduction_card && req.body.expiration_date) {
                    apiQuery(
                        'INSERT INTO customer_reductioncard (id_customer, id_reduction_card, valid_until) VALUES (?, ?, ?)',
                        [
                            customerInsertId,
                            req.body.id_reduction_card,
                            req.body.expiration_date
                        ],
                        function (reductionInsertion) {
                            if (reductionInsertion.err)
                                res.json({'id_customer': customerInsertId, 'err': reductionInsertion.err});
                            else
                                res.json({'id_customer': customerInsertId});
                        }
                    );
                }
                else
                    res.json({'id_customer': customerInsertId});
            }
        }
    );
});

//
// DELETE /api/customer/{id}
// Deletes a customer
//
// id: id of the customer to delete [required]
//
router.delete('/customer/:id', function (req, res) {
    apiQuery(
        'DELETE FROM customers WHERE id = ?',
        [req.params.id],
        function (r) {
            if (r.err) res.json({err: r.err});
            else res.json({});
        }
    );
});

//
// DELETE /api/ticket/{id}
// Deletes a ticket
//
// id: id of the ticket to delete [required]
//
router.delete('/ticket/:id', function (req, res) {
    apiQuery(
        'DELETE FROM tickets WHERE id = ?',
        [req.params.id],
        function (r) {
            if (r.err) res.json({err: r.err});
            else res.json({});
        }
    );
});

//
// PUT /api/ticket/{id}/state/{state}
// Updates the state of a ticket
//
// id: id of the ticket [required]
// state: the new state of the ticket (confirmed, pending, canceled) [required]
//
router.put('/ticket/:id/state/:state', function (req, res) {
    apiQuery(
      'UPDATE tickets SET state = ? WHERE id = ?',
      [req.params.state, (0 + req.params.id)],
      function (r) {
          if (r.err) res.json({err: r.err});
          else res.json({});
      }
    );
});

//
// GET /api/trains
// Returns train list
//
router.get('/trains', function (req, res) {
    sendQueryResult(res, 'SELECT * FROM trains_view');
});

//
// GET /api/train/{id}
// Returns train information
//
// id: id of the train [required]
//
router.get('/train/:id', function (req, res) {
    sendQueryResult(res, 'SELECT id, departure_time, departure_station, arrival_time, arrival_station FROM segments_view WHERE id_train = ?', [req.params.id]);
});

//
// GET /api/price/{departureSegment}-{arrivalSegment}/{class}
// Returns the price of a journey
//
// departureSegment: id of the departure segment [required]
// arrivalSegment: id of the arrival segment [required]
// class: class number of the coach [required]
//
// Query string parameters:
//   reduction: id of the reduction [optional]
//   customer: id of the customer [optional]
//
// Examples :
//   /price/1-2/2
//   /price/2-3/2?reduction=1
//   /price/2-3/2?reduction=1&customer=1
//
router.get('/price/:departureSegment-:arrivalSegment/:class', function (req, res) {
    sendQueryResult(res, 'CALL get_ticket_price(?,?,?,?,?,@price); SELECT @price AS price;',
    [
        req.params.departureSegment,
        req.params.arrivalSegment,
        req.params.class,
        req.query.customer,
        req.query.reduction
    ],
    function (r) {
        if (r.err) return {'err': r.err};
        return r[1][0];
    });
});

//
// GET /api/seats/available/{departureSegment}-{arrivalSegment}
// Returns the number of available seats between two segments of a journey
//
// departureSegment: id of the departure segment [required]
// arrivalSegment: id of the arrival segment [required]
//
// Query string parameters:
//   class: class number [optional]
//   situation: seat situation (window, aisle) [optional]
//
router.get('/seats/available/:departureSegment-:arrivalSegment', function (req, res) {
    var query = 'CALL get_available_seats(?,?); SELECT COUNT(*) AS nb_available_seats FROM available_seats WHERE 1';
    var queryParams = [req.params.departureSegment, req.params.arrivalSegment];
    if (req.query.class) {
        query += ' AND num_class=?';
        queryParams.push(req.query.class);
    }
    if (req.query.situation) {
        query += ' AND situation=?';
        queryParams.push(req.query.situation);
    }
    query += ';';
    sendQueryResult(res, query,
    queryParams,
    function (r) {
        if (r.err) return {'err': r.err};
        return r[1][0];
    });
});

//
// POST /api/book
// Adds a ticket. Returns the id of the new ticket.
//
// Post parameters
//   id_departure_segment: id of the departure segment [required]
//   id_arrival_segment: id of the arrival segment [required]
//   class: class number [required]
//   id_customer: id of the customer [required]
//   situation: seat situation [optional]
//   id_reduction: id of the reduction [optional]
//
router.post('/book', function (req, res) {
    if (!req.body.id_departure_segment || !req.body.id_arrival_segment || !req.body.class || !req.body.id_customer) {
        res.json({err: 'wrong inputs'});
        return;
    }

    sendQueryResult(res, 'CALL create_ticket(?,?,?,?,?,?,@insert_id); SELECT @insert_id AS insert_id;',
    [
        req.body.id_departure_segment,
        req.body.id_arrival_segment,
        req.body.class,
        req.body.situation,
        req.body.id_customer,
        req.body.id_reduction
    ],
    function (r) {
        if (r.err) return {'err': r.err};
        return r[1][0];
    });
});
