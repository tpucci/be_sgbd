var mysql = require('mysql');
module.exports = function(){
    console.log(process.env.NODE_ENV +' environment configuration');
    switch(process.env.NODE_ENV){
        case 'development':
            return mysql.createPool( require('./config/database') );

        case 'production':
            return mysql.createPool( require('./config/database_prod') );

        default:
            return mysql.createPool( require('./config/database') );
    }
};

//module.exports = mysql.createPool( require('./config/database') );
