-- Fix

DROP PROCEDURE IF EXISTS is_seat_available;
DROP PROCEDURE IF EXISTS get_available_seats;

DELIMITER $$
CREATE PROCEDURE is_seat_available(IN departure_segment INT, IN arrival_segment INT, IN coach_seat INT, IN ticket_id_to_ignore INT, OUT available BOOLEAN)
BEGIN
  -- we assume departure_segment and arrival_segment are from the same trains

  SELECT id_train INTO @idTrain FROM segments WHERE id = departure_segment;

  SELECT id_train INTO @idTrain2 FROM segments WHERE id = arrival_segment;

  IF @idTrain != @idTrain2 OR @idTrain NOT IN (SELECT train_coaches.id_train FROM train_coaches INNER JOIN coach_seats ON coach_seats.id_coach = train_coaches.id_coach WHERE coach_seats.id = coach_seat) THEN
    SET available = FALSE;

  ELSE

    SELECT COUNT(tickets.id) INTO @nbTickets  FROM tickets
    INNER JOIN segments s1 ON s1.id = tickets.id_departure_segment
    INNER JOIN segments s2 ON s2.id = tickets.id_arrival_segment
    WHERE
    tickets.state != 'canceled'
    AND
    s1.id_train = @idTrain
    AND
      ((s2.arrival_time - (SELECT departure_time FROM segments WHERE segments.id = departure_segment))
      *
      (s1.departure_time - (SELECT arrival_time FROM segments WHERE segments.id = arrival_segment)) < 0)
    AND tickets.id_coach_seat = coach_seat
    AND tickets.id != ticket_id_to_ignore;

    IF @nbTickets = 0
    THEN SET available = TRUE;
    ELSE SET available = FALSE;
    END IF;
  
  END IF;
END$$

CREATE PROCEDURE get_available_seats(IN departure_segment INT, IN arrival_segment INT)
BEGIN
  DROP TABLE IF EXISTS available_seats;
  CREATE TEMPORARY TABLE available_seats AS(
  SELECT coach_seats.id AS id_coach_seat, coaches.num_class, coach_seats.situation FROM coach_seats
  INNER JOIN train_coaches ON train_coaches.id_coach = coach_seats.id_coach
  INNER JOIN coaches ON coaches.id = coach_seats.id_coach
  WHERE train_coaches.id_train = (SELECT id_train FROM segments WHERE id = departure_segment)
  AND train_coaches.id_train = (SELECT id_train FROM segments WHERE id = arrival_segment)
  AND coach_seats.id NOT IN 
  (
    SELECT coach_seats.id AS id_coach_seat FROM tickets
    INNER JOIN segments s1 ON s1.id = tickets.id_departure_segment
    INNER JOIN segments s2 ON s2.id = tickets.id_arrival_segment
    INNER JOIN coach_seats ON coach_seats.id = tickets.id_coach_seat
    WHERE
    tickets.state != 'canceled'
    AND
    s1.id_train = (SELECT id_train FROM segments WHERE segments.id = departure_segment)
    AND
    ((s2.arrival_time - (SELECT departure_time FROM segments WHERE segments.id = departure_segment))
    *
    (s1.departure_time - (SELECT arrival_time FROM segments WHERE segments.id = arrival_segment)) < 0)
  ));
END$$
DELIMITER ;

GRANT EXECUTE ON PROCEDURE `trains_BE_2016`.`get_available_seats` TO 'train_ticket_booking_manager'@'%';
GRANT EXECUTE ON PROCEDURE `trains_BE_2016`.`is_seat_available` TO 'train_ticket_booking_manager'@'%';
