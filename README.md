# BE SGBD - Agence de voyages SNCF

Ce README documente toutes les étapes nécessaires pour que l'application soit déployée en développement sur un réseau local.

----

## Dépendances Systèmes

- Base de données MySQL
- Node.js et NPM

----

## Configuration

### Initialisation de la base de données

Un serveur MySQL doit être installé sur le poste. Nous recommandons la version 5.7.17. Se référer à [http://dev.mysql.com/downloads/mysql/](http://dev.mysql.com/downloads/mysql/) pour l'installation du serveur.

Avec le superutilisateur (root), exécuter le code SQL donné dans le fichier `database_init.sql`. Ce fichier crée la base de données, l'utilisateur lié à notre application et rempli les tables avec des données pour pouvoir tester l'application.

### Installation de NodeJs et NPM

NodeJs et NPM doivent être installés sur le poste. Nous recommandons la version 6.9.3 de NodeJs. Se référer à [https://nodejs.org/en/download/](https://nodejs.org/en/download/) pour l'installation.

### Installation des dépendances

Ouvrir un terminal et se rendre dans le dossier racine l'application (contenant notamment le fichier package.json). Lancer la commande suivante :
```Bash
npm install
```

Les dépendances de l'application vont alors être installées.

### Configuration de la connexion à la base de données

Éditer le fichier `config/database.js` et changer la valeur du champ `port` en fontion du port du serveur MySQL.
```Javascript
// Database configuration

module.exports = {
    connectionLimit : 10,
    host     : 'localhost',
    port     : '32768',
    user     : 'train_ticket_booking_manager',
    password : 'train_ticket_booking_manager_password',
    database : 'trains_BE_2016',
    charset  : 'utf8_general_ci',
    debug    :  false,
    multipleStatements: true
};
```

### Lancement du serveur

1. Utiliser la commande suivante pour lancer le serveur de développement :
```Bash
npm start
```

Un serveur web est alors créé sur le port 8080. Pour changer ce port, modifier la variable d'environnement PORT, par exemple :
```Bash
PORT=8080 npm start
```
2. Visiter l'adresse [http://localhost:8080](http://localhost:8080) : l'application doit fonctionner sans erreur.

---

## Utilisation de l'application

### Section Trains

Cette section charge un composant principal `Trains.js` qui affiche la liste des trains de la base de données. Pour chacun d'entre eux, il est possible d'afficher la liste détaillée des gares sur le trajet du train. En sélectionnant deux gares différentes, un panneau de réservation affiche les places disponibles sur le trajet entre ces deux gares pour le train sélectionné. Le bouton "Réserver" apparaît si des places sont disponibles. Le cas échéant, "Réserver" permet d'afficher le formulaire de réservation du trajet choisi.

![Trains](readme/trains.gif)

### Section Billets

Cette section charge un composant principal `Billet.js` qui affiche la liste des billets de la base de donées. Un clic sur l'un d'entre eux permet d'afficher les informations détaillées du billet sélectionné dans le panneau contextuel de droite. Dans ce panneau, il est possible de modifier l'état de réservation ou de supprimer le billet de la base de donnée.

![Trains](readme/billets.gif)

### Section Clients

Cette section charge un composant principal `Clients.js` qui affiche la liste des clients de la base de données. Un clic sur l'un d'entre eux permet d'afficher les informations détaillées du client sélectionné dans le panneau contextuel de droite. Dans ce panneau, il est possible de voir la liste des billets attachés à ce client et de supprimer le client de la base de donnée.

![Trains](readme/clients.gif)

### Section Voitures

Cette section fait partie de la section "État", et ne permet pas d'interragir avec la base de donnée. Ce choix est justifié car nous modélisons une agence de voyage SNCF : l'agence ne gère pas la logistique des transports mais seulement la vente et la relation client.
Cette section charge un composant principal `Voitures.js` qui affiche la liste des voitures de la base de données. Un clic sur l'un d'entre eux permet d'afficher les informations détaillées de la voiture sélectionnés dans le panneau contextuel de droite. Dans ce panneau, il est possible de voir la liste des trains auquel la voiture est attachée.

### Section Réductions

Cette section fait partie de la section "État", et ne permet pas d'interragir avec la base de donnée. Ce choix est justifié car nous modélisons une agence de voyage SNCF : l'agence ne gère pas la logistique et le marketing des transports mais seulement la vente et la relation client.
Cette section charge un composant principal `Reductions.js` qui affiche la liste des réductions de la base de données.


---

