DROP PROCEDURE IF EXISTS get_ticket_price;
DROP PROCEDURE IF EXISTS create_ticket;

DELIMITER $$

CREATE PROCEDURE get_ticket_price(IN departure_segment INT, IN arrival_segment INT, IN class SMALLINT, IN customer INT, IN id_reduction INT, OUT price DOUBLE)
BEGIN
  DECLARE initial_price DOUBLE;
  DECLARE class_price_rate DOUBLE;
  DECLARE reduction_price_rate DOUBLE;
  DECLARE reduction_card_price_rate DOUBLE;

  SELECT SUM(segments.initial_price) INTO initial_price
  FROM segments
  INNER JOIN segments dep_seg ON dep_seg.id_train = segments.id_train
  INNER JOIN segments arr_seg ON arr_seg.id_train = segments.id_train
  WHERE dep_seg.id = departure_segment
  AND arr_seg.id = arrival_segment
  AND segments.departure_time >= dep_seg.departure_time
  AND segments.arrival_time <= arr_seg.arrival_time;

  SELECT price_rate INTO class_price_rate
  FROM classes
  WHERE num = class
  LIMIT 1;

  SELECT price_rate INTO reduction_price_rate
  FROM reductions
  WHERE id = id_reduction
  LIMIT 1;

  SELECT price_rate INTO reduction_card_price_rate
  FROM reduction_cards
  INNER JOIN customer_reductioncard ON customer_reductioncard.id_reduction_card = reduction_cards.id
  WHERE customer_reductioncard.id_customer = customer AND customer_reductioncard.valid_until>=NOW()
  LIMIT 1;
  
  IF reduction_price_rate      IS NULL THEN SET reduction_price_rate = 1;      END IF;
  IF reduction_card_price_rate IS NULL THEN SET reduction_card_price_rate = 1; END IF;
  
  SET price = ROUND(initial_price*class_price_rate*reduction_price_rate*reduction_card_price_rate*100)/100;
END$$

CREATE PROCEDURE create_ticket(IN departure_segment INT, IN arrival_segment INT, IN class INT, IN sit ENUM('window','aisle'), IN customer INT, IN reduction INT, OUT insert_id INT)
BEGIN
  CALL get_available_seats(departure_segment, arrival_segment);
  SET @coach_seat_to_book = NULL;
  IF sit IS NOT NULL THEN
    SELECT id_coach_seat INTO @coach_seat_to_book FROM available_seats WHERE num_class=class AND situation=sit ORDER BY id_coach_seat LIMIT 1;
  ELSE
    SELECT id_coach_seat INTO @coach_seat_to_book FROM available_seats WHERE num_class=class ORDER BY id_coach_seat LIMIT 1;
  END IF;

  IF @coach_seat_to_book IS NULL THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='no more available seats';
  ELSE

    CALL get_ticket_price(departure_segment, arrival_segment, class, customer, reduction, @price);
    IF @price IS NOT NULL THEN
      INSERT INTO `tickets` (`id`, `id_customer`, `id_departure_segment`, `id_arrival_segment`, `id_coach_seat`, `price`, `state`)
      VALUES (NULL, customer, departure_segment, arrival_segment, @coach_seat_to_book, @price, 'pending');
      
      SELECT id INTO insert_id FROM tickets WHERE id_customer=customer AND id_coach_seat=@coach_seat_to_book AND id_departure_segment=departure_segment AND id_arrival_segment=arrival_segment LIMIT 1;
    END IF;
  END IF;
END$$

DELIMITER ;

GRANT EXECUTE ON PROCEDURE `trains_BE_2016`.`get_ticket_price` TO 'train_ticket_booking_manager'@'%';
GRANT EXECUTE ON PROCEDURE `trains_BE_2016`.`create_ticket` TO 'train_ticket_booking_manager'@'%';

