// Database configuration

module.exports = {
    connectionLimit : 10,
    host     : 'localhost',
    port     : '32768',
    user     : 'train_ticket_booking_manager',
    password : 'train_ticket_booking_manager_password',
    database : 'trains_BE_2016',
    charset  : 'utf8_general_ci',
    debug    :  false,
    multipleStatements: true
};
