# Introduction
Ce document rend compte du travail réalisé au cours des trois Bureaux d'Étude de systèmes de bases de données. Ce travail a consisté à concevoir et réaliser un __système de réservation de billets pour une agence de voyages SNCF__.

Le système développé doit permettre aux agents de réservation SNCF d'effectuer les tâches essentielles qui leur incombent : enregistrement de clients, consultation des horaires des trains, création de billets, etc. La configuration des trains (nombre de places, ...), leur planification (gestion des trajets et des horaires) et la politique commerciale (constitution des tarifs et des réductions) sont donc hors de portée de notre système de réservation et pourraient faire l'objet de logiciels dédiés. Pour pouvoir manipuler notre solution, nous avons proposé un exemple d'intialisation de ces données non modifiables par l'application.

La suite du présent doncument décrit nos démarches, les solutions que nous avons retenues et leurs motivations.

Le code source de l'application développée au cours de ces Bureaux d'Étude ainsi que les instructions d'installation sont fournies avec ce présent rapport. __L'application a aussi été mise en ligne à l'adresse : [https://goo.gl/IpSjlY](https://goo.gl/IpSjlY)__. Pour son installation en local, se référer à [https://goo.gl/91lUT5](https://goo.gl/91lUT5).

# Conception

Nous avons fait le choix de deux principes pour concevoir notre application :

- __non redondance__ : le modèle de données construit doit induire le moins de redondance possibles : l'intégrité des données nous a semblé plus importante que la performance dans le cas étudié, les relations répondront donc toujours à la forme normale 3 (3NF);
- __concentration des traitements dans la base de données__ : les traitements (récupérations, modifications, vérifications) sont réalisés le plus possible par la base de données. Ce choix n'est sans doute pas pertinent en temps normal puisqu'il est en général plus facile de maintenir le code source du serveur web, cependant nous voulions appliquer de façon la plus exhaustive possible les principes vus en cours de systèmes de bases de données.



## Architecture matérielle
Étant donné que nous concentrons les traitements au niveau de la base de données, nous proposons d'utiliser un serveur dédié pour héberger le serveur MySQL.

Le serveur web quand à lui n'a pas besoin d'être très puissant d'un point de vue mémoire ou temps de calcul. Nous mettons plutôt l'accent sur la bande passante qu'il partage avec le serveur de bases de données, qui doit être suffisante pour permettre un grand nombre de connexions et un échange intensif de données.

Les postes clients des agents de voyages SNCF n'ont besoin d'aucune modification par rapport à l'existant (un navigateur web moderne et une connexion au réseau interne suffisent).

## Architecture logicielle

### Principe général
La solution proposée se découpe en trois parties majeures :

- l'interface web, qui s'occupe de la présentation des données et des interactions avec l'utilisateur final ; elle est exécutée sur les postes clients (agents de voyages) ;
- le serveur web, qui interprète les requêtes faites via l'interface web et dialogue avec la base de données ;
- le serveur MySQL, qui stocke et effectue les traitements sur les données.

![Architecture générale](res/architecture.pdf)

### Langages et technologies
Nous avons choisi de développer une application web pour implémenter notre solution. En effet, avec l'accroissement rapide des possibilités offertes par le web, de plus en plus d'applications métier sont développées sous cette forme. Elles ont l'avantage de ne pas nécessiter d'installation particulière auprès des postes clients ; elles sont en outre très flexibles vis-à-vis du système d'information cible et facilement maintenables.

La partie client de l'application est développée en HTML5 et Javascript avec les frameworks React et Materialize. L'utilisation de React simplifie la gestion des interactions utilisateur et donne un cadre moderne de développement. Materialize est utilisé pour avoir une mise en forme de type Material Design.

La partie serveur est développée en Javascript (NodeJs) avec le framework Express. NodeJs a l'avantage d'être léger et exrêmement rapide, d'avoir une communauté dynamique et possède un système de gestion de dépendances efficace. Programmer dans le même langage sur la partie serveur et client est en outre agréable.

La partie base de données est évidemment développée en SQL et utilise un serveur MySQL.

### Définition des interfaces

Les différentes vues de l'interface sont les suivantes :

- la liste des trains, avec pour chaque train la liste des arrêts avec horaires (cette vue permet aussi d'initier une réservation) ;
- la réservation d'une place ;
- la liste des billets avec leurs détails ;
- la liste des clients ;
- pour chaque client, la liste de ses billets ;
- l'ajout d'un client ;
- la liste des voitures avec sa classe et son nombre de sièges ;
- pour chaque voiture, le détail de la situation des sièges et la liste des trains utilisant la voiture ;
- la liste des réductions.

## Modèle des données

Nous avons identifié les éléments et les interactions suivantes :

- les __clients__, qui possèdent _au plus_ une __carte de réduction__ et qui achètent des __billets de train__ ;
- des __réductions__ peuvent s'appliquer aux __billets de train__ ;
- un __billet de train__ concerne _un et un seul_ __siège__ et est lié à _deux_ __segments de voyage__  (départ et arrivée ; il peuvent être identiques si le billet ne concerne qu'un segment) d'un _même_ __train__ ;
- un __segment de voyage__ est parcouru par _un et un seul_ __train__ (chaque train possède ses propres segments de voyage) ; un segment relie _deux_ __gares__ (départ, arrivée) ;
- un __siège__ est attaché à _une et une seule_ __voiture__ ;
- une __voiture__ est caractérisée par sa __classe__ ;
- une __voiture__ est affectée à _zéro, un ou plusieurs_ __trains__ (à condition que les trains qui emploient une même voiture circulent à des moments différents).

Un __train__ et ses __segments de voyage__ sont donc à usage unique (pour chaque voyage, un train et les segments correspondant aux arrêts du train doivent être créés). Une __voiture__ et ses __sièges__ représentent physiquement ce qu'ils désignent, ils sont donc réutilisés tant qu'ils existent physiquement.

\clearpage
Le modèle Entité-Relations est le suivant :

![Modèle Entité-Relations](res/E-R.pdf)

Les tables et les relations sont données dans le fichier d'initialisation de la base de données.
Néanmoins, la liste des tables et de leurs colonnes est la suivante (les clés primaires sont soulignées, les clés étrangères sont en italique) :

- __classes__ : <u>num</u> (numéro de class), price_rate (facteur de tarification)
- __coaches__ : <u>id</u>, _num_class_ (référence une classe [classes.num])
- __coach_seats__ : <u>id</u>, _id_coach_ (référence une voiture [coaches.id]), seat_num (numéro de siège dans la voiture), situation (couloir, fenêtre)
- __customers__ : <u>id</u>, first_name, last_name, brithdate
- __customer_reductioncard__ : _id_customer_ (référence un client [customers.id]), _id_reduction_card_ (référence une carte de réduction [reduction_cards.id]), valid_until (date de fin de validité de la carte)
- __reductions__ : <u>id</u>, name, price_rate (facteur de tarification)
- __reduction_cards__ : <u>id</u>, name, price_rate (facteur de tarification)
- __segments__ : <u>id</u>, _id_train_ (référence un train [trains.id]), _id_departure_station_ (référence un gare de départ [stations.id]), _id_arrival_station_ (référence un gare d'arrivée [stations.id]), departure_time, arrival_time, initial_price (prix sans réduction)
- __stations__ : <u>id</u>, name
- __tickets__ : <u>id</u>, _id_customer_ (référence un client [customers.id]), _id_departure_segment_ (référence un segment de départ [segments.id]), _id_arrival_segment_ (référence un segment d'arrivée [segments.id]), _id_coach_seat_ (référence un siège [coach_seats.id]), price (prix calculé), state (en attente, confirmé, annulé)
- __ticket_reduction__ : _id_ticket_ (référence un ticket [tickets.id]), _id_reduction_ (référence une réduction [reductions.id])
- __trains__ : <u>id</u>
- __train_coaches__ : _id_train_ (référence un train [trains.id]), _id_coach_ (référence une voiture [coaches.id]), coach_num (numéro de voiture dans le train)

Remarques :

- les tables __customer_reductioncard__, __ticket_reduction__ et __train_coaches__ modélisent des relations ;
- la table __trains__ ne contient qu'une colonne id, elle semble donc inutile mais il nous a semblé intéressant de déider une table aux trains pour de futures évolutions (si l'on souhaite préciser le type de train, etc.) ;
- la table __tickets__ possède une colonne de prix, ce qui peut sembler redondant car le prix peut être retrouvé avec les informations du ticket. Cependant, nous pensons qu'une fois le ticket émis, son prix doit rester fixe même si des modifications tarifaires ont eu lieu après la date d'achat ;
- l'état d'un billet (colonne state) peut uniquement passer de :
    - "en attente" à "confirmé" ou "annulé" ;
    - "confirmé" à "annulé".

# Réalisation

## Méthodologie de développement
Nous avons adopté une méthode de développement agile pour optimiser notre productivité. Nous avons utilisé Trello pour organiser notre travail et Git pour collaborer et versionner le code.

## Front-end

Le front-end se présente sous la forme d'une application [React](https://facebook.github.io/react/). Cette application emploie les nouvelles technologies de présentation javascript JSX. Le but de ce rapport n'est pas l'explication détaillée de l'application Front-end. Néanmoins, nous tenons à expliquer ici les principaux composants de l'application.

### A propos de React

React est une librairie Javascript pour construire des interfaces utilisateur. C'est une librairie déclarative et basée sur la création de composants (ce sont des vues changeantes en fonction de leurs états et des propriétés reçues de leurs parents).
Dans notre cas, nous utilisons une librairie supplémentaire de composants prédéfinis nommée [Material UI](http://www.material-ui.com/).

Il est important de noter que l'application finale est une "single-page app". Cela signifie qu'une fois la page chargée, le navigateur ne changera pas d'URL mais naviguera de manière transparente entre les états des composants : les seuls appels sont émis à un point d'accès (API) et sont traités de manière asynchrone.
Pour cela, nous créons une librairie locale capable d'appeler l'API (décrite ci-après dans la section back-end).

### Menu et container principaux

Le menu de gauche, l'affichage principal ainsi que le panneau de droite contextuel sont une première catégorie de composants React. Ils sont définis et apparaissent au plus haut niveau de l'application React.
Le menu de gauche intègre le routeur. Un clic sur l'un des éléments de la liste recharge le container principal dans l'espace central.

### Section Trains

Cette section charge un composant principal `Trains.js` qui affiche la liste des trains de la base de données. Pour chacun d'entre eux, il est possible d'afficher la liste détaillée des gares sur le trajet du train. En sélectionnant deux gares différentes, un panneau de réservation affiche les places disponibles sur le trajet entre ces deux gares pour le train sélectionné. Le bouton "Réserver" apparaît si des places sont disponibles. Le cas échéant, "Réserver" permet d'afficher le formulaire de réservation du trajet choisi.

### Section Billets

Cette section charge un composant principal `Billet.js` qui affiche la liste des billets de la base de donées. Un clic sur l'un d'entre eux permet d'afficher les informations détaillées du billet sélectionné dans le panneau contextuel de droite. Dans ce panneau, il est possible de modifier l'état de réservation ou de supprimer le billet de la base de données.

### Section Clients

Cette section charge un composant principal `Clients.js` qui affiche la liste des clients de la base de données. Un clic sur l'un d'entre eux permet d'afficher les informations détaillées du client sélectionné dans le panneau contextuel de droite. Dans ce panneau, il est possible de voir la liste des billets attachés à ce client et de supprimer le client de la base de données.

### Section Voitures

Cette section fait partie de la section "État", et ne permet pas d'interragir avec la base de données. Ce choix est justifié car nous modélisons une agence de voyages SNCF : l'agence ne gère pas la logistique des transports mais seulement la vente et la relation client.
Cette section charge un composant principal `Voitures.js` qui affiche la liste des voitures de la base de données. Un clic sur l'un d'entre eux permet d'afficher les informations détaillées de la voiture sélectionnée dans le panneau contextuel de droite. Dans ce panneau, il est possible de voir la liste des trains auxquels la voiture est attachée.

### Section Réductions

Cette section fait partie de la section "État", et ne permet pas d'interragir avec la base de donnée. Ce choix est justifié car nous modélisons une agence de voyage SNCF : l'agence ne gère pas la logistique et le marketing des transports mais seulement la vente et la relation client.
Cette section charge un composant principal `Reductions.js` qui affiche la liste des réductions de la base de données.

\clearpage

## Back-end

Le back-end de notre application se décompose en deux entités :

- un serveur web NodeJs exposant les services sous forme d'une API ;
- un serveur MySQL.

La partie web est relativement simple car elle s'occupe uniquement de dialoguer avec le serveur MySQL : un minimum de traitements y est effectué et les résultats sont renvoyés sous la forme d'objets JSON (les données ne sont donc pas présentées pour l'utilisateur, la mise en forme des résultats est faite dans la partie front-end). Les requêtes envoyées par le serveur web au serveur MySQL sont visibles dans la sortie standard du serveur web.

Le code source du serveur web est décomposé de la manière suivante :

- `app.js`, point d'entrée du serveur ;
- `api.js`, qui traite les requêtes clients et effectue les requêtes vers la base de données ;
- `mysqlpool.js`, qui crée un pool de connexions à la base de données ;
- `config/database.js`, la configuration de l'accès à la base de données.

### API web
Pour chaque transaction prévue (voir ci-après), accepte une requête HTTP.

À titre d'exemple, le code de l'API donnant accès aux informations sur un train est donné ci-dessous :
```Java
//
// GET /api/train/{id}
// Returns train information
//
// id: id of the train [required]
//
router.get('/train/:id', function (req, res) {
    sendQueryResult(res, 'SELECT id, departure_time, departure_station, arrival_time, arrival_station FROM segments_view WHERE id_train = ?', [req.params.id]);
});
```

Pour obtenir les information sur le train 1, l'interface enverra donc une requête HTTP GET à /api/train/1. La réponse, qui contient la liste des segments de voyage parcourus par le train, sera de la forme :
```Java
[
    {
        "id":1,
        "departure_time":"2016-12-16T07:30:00.000Z",
        "departure_station":"Nice",
        "arrival_time":"2016-12-16T09:30:00.000Z",
        "arrival_station":"Marseille"
    },
    {
        "id":2,
        "departure_time":"2016-12-16T09:30:00.000Z",
        "departure_station":"Marseille",
        "arrival_time":"2016-12-16T11:30:00.000Z",
        "arrival_station":"Montpellier"
    }
]
```

### Administration et accès à la base de données
Dans le cadre de notre travail, la base de données possède deux utilisateurs :

- l'administrateur, utilisé pour initialiser la base ;
- `train_ticket_booking_manager`, utilisé par le serveur web pour accéder à la base.

Les droits conférés à `train_ticket_booking_manager` sont limités aux opérations prévues par l'interface web. Cet utilisateur ne pourra pas par exemple modifier la planification des trains, les tarifs, les réductions ni même créer "à la main" des billets (afin de ne pas pouvoir compromettre le prix des billets, la création des billets se fait via une procédure et la modification est limitée à l'état du billet). Ses droits sont listés ci-dessous :

- requêtes de type `SELECT` vers toutes les tables de la base de données ;
- ajout, modification et suppression de lignes des tables __ticket_reduction__, __customer_reductioncard__ et __customers__ ;
- modification de la colonne 'state' et suppression de lignes de la table __tickets__ ;
- execution de toutes les procédures stockées.

### Tables et dépendances
Les tables ont été créées à l'aide de phpMyAdmin. Des clés uniques ont été utilisées pour contraindre le modèle et des index ont été ajoutés à certaines colonnes pour augmenter la performance de certaines requêtes.

Le code SQL de génération des tables et de leurs dépendances est donné avec le fichier `database_init.sql`.

### Transactions
Les transactions souhaitées entre l'interface et la base de données sont les suivantes :

- obtenir la liste des réductions ;
- obtenir la liste des voitures avec leur classe et leur capacité ;
- obtenir les informations détaillées sur une voiture (situation des sièges, trains et trajets rattachés) ;
- obtenir la liste des clients avec leur carte de réduction le cas échéant ;
- obtenir les informations détaillées sur un client (y compris les trajets qu'il a effectués) ;
- obtenir la liste des billets (avec leurs informations détaillées) ;
- ajouter d'un client ;
- supprimer d'un client ;
- supprimer d'un billet ;
- changer de l'état d'un billet ;
- obtenir la liste des trains (avec leurs gares et horaires de départ/arrivée) ;
- obtenir les informations détaillées sur un train (avec le détail des segments de voyage parcourus) ;
- obtenir le prix d'un billet en fonction d'un segment de départ, d'un segment d'arrivée, de la classe, d'une réduction éventuelle et du client éventuel
- obtenir la liste des sièges libres entre deux segments en fonction de la classe et de la situation éventuelles ;
- ajouter un billet à partir d'un segment de départ, d'un segment d'arrivée, de la classe, d'un client, d'une réduction optionnelle et d'une situation de siège optionnelle (le siège est ensuite choisi automatiquement parmis les sièges libres).

Les vues suivantes ont été créées pour simplifier les requêtes d'obtention des informations, elles résultent principalement de jointures entre les tables :

- __coaches_view__
- __coach_seats_view__
- __coach_travels_view__
- __customers_view__
- __nb_seats_by_coach__
- __segments_view__
- __tickets_customer_view__
- __tickets_view__
- __trains_view__

### Déclencheurs, vérifications et procédures
Comme les contraintes 'CHECK' ne sont pas implémentées par MySQL, créé des déclencheurs et des procédures pour vérifier la validité des données à l'ajout et à la modification. Nous avons écrit les déclencheurs suivants :

- `check_reductions_insert_trigger`, `check_reductions_update_trigger`, `check_red uction_cards_insert_trigger` et `check_reduction_cards_update_trigger` qui vérifient via la procédure `price_rate_is_reduction` que les facteurs de tarification des réductions sont bien inférieurs à 1 ;
- `check_segments_insert_trigger` et `check_segments_update_trigger` qui vérifient via la procédure `departure_is_before_arrival` que les dates d'arrivés des segments de voyages sont bien ultérieures aux dates de départ ;
- `check_tickets_insert_trigger`et `check_tickets_update_trigger` qui vérifient via les procédures `is_ticket_valid` et `is_ticket_new_state_valid` la validité des tickets.

Pour faciliter, alléger et factoriser les requêtes, d'autres procédures ont été écrites :

- `is_seat_available`, qui détermine si un siège est libre ;
- `get_available_seats`, qui donne les sièges disponibles entre deux segments de voyage ;
- `get_ticket_price`, qui calcule le prix d'un billet ;
- `create_ticket`, qui crée un billet.

# L'application finale
Nous donnons ci-dessous une copie d'écran de l'application finale.

![Aperçu de l'application](res/application.png)

__L'application est consultable à l'adresse suivante : [https://goo.gl/IpSjlY](https://goo.gl/IpSjlY)__. Pour son installation en local, se référer à [https://goo.gl/91lUT5](https://goo.gl/91lUT5).

# Conclusion
Le présent projet, réalisé au cours de trois Bureaux d'Étude, nous a permis de mettre en pratique la conception de bases de données et différentes notions vues pendant le cours de Systèmes de Bases de Données.

Afin de réutiliser au maximum les notions du cours, nous avons codé la plupart des traitements au niveau de la base de données, et non pas sur le serveur web comme il est plus courant de le faire. Si nous avions à réaliser un tel projet dans un contexte de production, nous délèguerions une partie de ces traitements au serveur web. En effet, il n'est pas recommandé de concentrer les traitements dans la base de données de manière à soulager le serveur. Aussi, par soucis de maintenabilité, nous utiliserions plutôt un ORM (object-relational mapping), au lieu d'effectuer des requêtes à la main pour récuperer et modifier les informations de la base.

Ce projet a été transverse : il nous a permi d'écrire une application web riche reposant sur des composants modernes massivement adoptés en développement web ; en outre, il a été intéressant d'un point de vue organisationnel car nous avons mis en pratique ses méthodes de développement agiles et utilisé Git pour collaborer sur le même code source.