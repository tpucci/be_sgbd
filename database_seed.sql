UPDATE `segments`SET `departure_time`='2016-12-16 08:30:00', `arrival_time`='2016-12-16 10:30:00'  WHERE `id`=1;
UPDATE `segments`SET `departure_time`='2016-12-16 10:30:00', `arrival_time`='2016-12-16 12:30:00'  WHERE `id`=2;
UPDATE `segments`SET `departure_time`='2016-12-16 10:00:00', `arrival_time`='2016-12-16 13:00:00'  WHERE `id`=3;
UPDATE `segments`SET `departure_time`='2016-12-16 13:00:00', `arrival_time`='2016-12-16 16:00:00'  WHERE `id`=4;
UPDATE `segments`SET `departure_time`='2016-12-16 16:00:00', `arrival_time`='2016-12-16 19:00:00'  WHERE `id`=5;

INSERT INTO `segments` (`id`, `id_train`, `id_departure_station`, `departure_time`, `id_arrival_station`, `arrival_time`, `initial_price`) VALUES
(6, 3, 8, '2016-12-24 08:30:00', 6, '2016-12-24 10:35:00', 10),
(7, 3, 6, '2016-12-24 10:35:00', 5, '2016-12-24 12:30:00', 12),
(8, 4, 9, '2016-12-30 11:09:00', 10, '2016-12-30 13:00:00', 10),
(9, 4, 10, '2016-12-30 13:00:00', 8, '2016-12-30 16:00:00', 13),
(10, 4, 8, '2016-12-30 16:00:00', 7, '2016-12-30 19:23:00', 15);

INSERT INTO `train_coaches` (`id_train`, `id_coach`, `coach_num`) VALUES
(1, 4, 2),
(2, 5, 2),
(3, 5, 2),
(4, 2, 2);