import React, {Component} from 'react';
import SideNav from './SideNav';

let toggleDrawer = () => {return -1};
let toggleSecondaryDrawer = () => {return -1};

export default class Main extends Component {

    constructor(props) {
        super(props);
        this.state = {
            drawerOpen : true,
            secondaryDrawerOpen : false,
        };
    }

    componentWillMount() {
        toggleDrawer = () => {this.setState({drawerOpen: !this.state.drawerOpen})};
        toggleSecondaryDrawer = (bool) => {this.setState({secondaryDrawerOpen: bool})};
    }

    render() {
        var mainClass = "main-default";
        if (this.state.drawerOpen) {
            mainClass += " main-pushed-on-left";
        }
        if (this.state.secondaryDrawerOpen) {
            mainClass += " main-pushed-on-right";
        }
        return(
            <div className={mainClass}>
                <SideNav open={this.state.drawerOpen} />
                {this.props.children}
            </div>
        );
    }
}

export {toggleDrawer, toggleSecondaryDrawer};