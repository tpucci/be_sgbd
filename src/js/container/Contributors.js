import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import MenuItem from 'material-ui/MenuItem';
import {List, ListItem} from 'material-ui/List';
import Identity from 'material-ui/svg-icons/action/perm-identity';
import {cyan500} from 'material-ui/styles/colors';

export default class ContributorsDialog extends React.Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  render() {
    const actions = [
      <FlatButton
        label="Fermer"
        primary={true}
        onTouchTap={this.handleClose}
      />,
    ];

    return (
      <div>
        <MenuItem onTouchTap={this.handleOpen}>Contributeurs</MenuItem>
        <Dialog
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
        >
          <h3>Contributeurs</h3>
          <List>
            <ListItem
                primaryText="Thomas Pucci"
                leftIcon={<Identity color={cyan500} />}
                disabled={true}
            />
            <ListItem
                primaryText="Yann Vaginay"
                leftIcon={<Identity color={cyan500} />}
                disabled={true}
            />
          </List>
        </Dialog>
      </div>
    );
  }
}