import React, {Component} from 'react';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import Train from 'material-ui/svg-icons/maps/train';
import Voiture from 'material-ui/svg-icons/notification/airline-seat-recline-extra';
import Billet from 'material-ui/svg-icons/action/receipt';
import Reduction from 'material-ui/svg-icons/action/loyalty';
import Contact from 'material-ui/svg-icons/communication/contacts';
import Subheader from 'material-ui/Subheader';
import AppBar from 'material-ui/AppBar';
import Divider from 'material-ui/Divider';
import Contributors from './Contributors';

export default class SideNav extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Drawer open={this.props.open}>
                <AppBar title="Agence SNCF" showMenuIconButton={false} />
                <Subheader>Actions</Subheader>
                <MenuItem leftIcon={<Train />} href="#/trains" >Trains</MenuItem>
                <MenuItem leftIcon={<Billet />} href="#/billets" primaryText="Billets"/>
                <MenuItem leftIcon={<Contact />} href="#/clients" >Clients</MenuItem>
                <Divider />
                <Subheader>Etat</Subheader>
                <MenuItem leftIcon={<Voiture />} href="#/voitures">Voitures</MenuItem>
                <MenuItem primaryText="Réductions" leftIcon={<Reduction />} href="#/reductions" />
                <Divider />
                <Subheader>A propos</Subheader>
                <Contributors >Contributeurs</Contributors>
            </Drawer>
        );
    }
}