import React from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import InformationPanel from '../components/InformationPanel';
import BilletInfo from './BilletInfo';
import Avatar from 'material-ui/Avatar';
import Chip from 'material-ui/Chip';
import Euro from 'material-ui/svg-icons/action/euro-symbol';
import Pending from 'material-ui/svg-icons/action/watch-later';
import Confirmed from 'material-ui/svg-icons/action/check-circle';
import Canceled from 'material-ui/svg-icons/navigation/cancel';
import {cyan200, cyan500, green500, cyan800, grey400, lightBlack} from 'material-ui/styles/colors';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import LinearProgress from 'material-ui/LinearProgress';

import { browserHistory } from 'react-router';

import '../helpers/FormatMoney';
import fetch from '../helpers/Fetch';
import getDateKey from '../helpers/GetDateKey';
import '../helpers/PriceRateFormat';

const style = {
    wrapper: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip : {
        margin: 4
    },
    chipText : {
        fontSize : "13px"
    },
    chipIcon : {
        width : 14,
        height: 14,
        fill: "white"
    }
};

let setBilletState = (id, value)=>{return -1};

class BilletTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            targetBilletID : props.billetTargetId,
            billets : [],
            targetBillet: []
        };
    }

    componentWillMount() {
        this._getBillets();
        setBilletState = (id, value) => {
            let index = -1;
            for(let i = 0; i < this.state.billets.length; i++) {
                if (this.state.billets[i].id == id) {
                    index = i;
                    break;
                }
            }
            let newBillets = this.state.billets;
            newBillets[index].state = value;
            let newBillet = this.state.targetBillet;
            newBillet.state = value;
            this.setState({billets : newBillets, targetBillet : newBillet });
        };
    }

    _getBillets() {
        fetch("/api/tickets").then((billets) => {
            if (this.props.billetTargetId > -1) {
                let index = -1;
                for(let i = 0; i < billets.length; i++) {
                    if (billets[i].id == this.props.billetTargetId) {
                        index = i;
                        break;
                    }
                }
                this.setState({
                    billets: billets,
                    targetBillet: billets[index]
                });
            } else {
                this.setState({billets: billets});}
        })
        .catch((err) => {
            console.log("Erreur : "+err);
        });
    }

    _mapBillets() {
        return this.state.billets.map((billet) => {
            let state;
            switch(billet.state) {
                case "canceled":
                    state = (<span><Canceled style={{fill: grey400, float: "right"}} /> Annulé</span>);
                    break;
                case "pending":
                    state = (<span><Pending style={{fill: cyan800, float: "right"}}/> En attente</span>);
                    break;
                case "confirmed":
                    state = (<span><Confirmed style={{fill: green500, float: "right"}} /> Confirmé</span>);
                    break;
            }
            let placement = "-";
            if (billet.state !== "canceled") {
                placement = (<div>
                    <div>{billet.coach_num + " - " + billet.seat_num}</div>
                    <div style={{color: lightBlack}}>Classe {billet.class}</div>
                </div>);
            } 
            let train = (<div>
                            <div>{billet.departure_station} - {billet.arrival_station}</div>
                            <div style={{color: lightBlack}}>{getDateKey(billet.departure_time)}</div>
                        </div>);
            let reduction_card = null;
            if (billet.card_name != null) {
                reduction_card = (
                        <Chip style={style.chip}>
                            <Avatar size={32} style={style.chipText}>
                                {billet.card_name.match(/\b\w/g).join('')}
                            </Avatar>
                            <span style={style.chipText}>{billet.reduction_card_price_rate.priceRateFormat()}</span>
                        </Chip>
                );
            }
            let reduction_name = null;
            if (billet.reduction_name != null) {
                reduction_name = (
                        <Chip style={style.chip}>
                            <Avatar size={32} style={style.chipText}>
                                {billet.reduction_name.match(/\b\w/g).join('')}
                            </Avatar>
                            <span style={style.chipText}>{billet.reduction_price_rate.priceRateFormat()}</span>
                        </Chip>
                );
            }
            return(
                <TableRow key={billet.id} selected={this.state.targetBilletID == billet.id}>
                    <TableRowColumn><div style={{marginTop: 4}}>{billet.first_name} {billet.last_name}</div>
                        {reduction_card}
                    </TableRowColumn>
                    <TableRowColumn>{train}</TableRowColumn>
                    <TableRowColumn>{placement}</TableRowColumn>
                    <TableRowColumn>
                        <div style={style.wrapper}>
                            <Chip style={style.chip} backgroundColor={cyan200}>
                                <Avatar size={32} style={style.chipText} backgroundColor={cyan500}>
                                    <Euro style={style.chipIcon}/>
                                </Avatar>
                                <span style={style.chipText}>{billet.price.formatMoney()}</span>
                            </Chip>
                            {reduction_name}
                        </div>
                    </TableRowColumn>
                    <TableRowColumn>
                        {state}
                    </TableRowColumn>
                </TableRow>
            );
        });
    }

    _onRowSelection = (rows) => {
        if(rows.length > 0) {
            this.setState({
                targetBilletID: this.state.billets[rows[0]].id,
                targetBillet: this.state.billets[rows[0]]
            });
            browserHistory.push('#/billets/'+this.state.billets[rows[0]].id);
        } else {
            this.setState({targetBilletID: -1});
            browserHistory.push('#/billets');
        }
    };

    render() {
        const billets = this._mapBillets();
        if (billets.length == 0) return (<LinearProgress mode="indeterminate" />);

        return(
            <div>
                <Table onRowSelection={this._onRowSelection}>
                    <TableHeader displaySelectAll={false}>
                        <TableRow>
                            <TableHeaderColumn>Client</TableHeaderColumn>
                            <TableHeaderColumn>Train</TableHeaderColumn>
                            <TableHeaderColumn>Placement</TableHeaderColumn>
                            <TableHeaderColumn>Prix</TableHeaderColumn>
                            <TableHeaderColumn>Statut</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody deselectOnClickaway={false}>
                        {billets}
                    </TableBody>
                </Table>
                <InformationPanel target={this.state.targetBilletID} width={512}>
                    <BilletInfo target={this.state.targetBilletID} billet={this.state.targetBillet}/>
                </InformationPanel>
                <FloatingActionButton href="#/trains" style={{position: "absolute", bottom: 40, right: 40}} >
                    <ContentAdd />
                </FloatingActionButton>
            </div>
        );
    }
}

export default BilletTable;
export {setBilletState};