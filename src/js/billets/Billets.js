import React from 'react';
import AppBar from '../components/AppBar';
import BilletTable from './BilletTable';

const styleContentItem = {
    flex: "0 0 auto"
};

const styleContentMain = {
    overflowY: "scroll",
    overflowX: "hidden",
    flex: "1 1 auto"
};

class Billets extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            billetTargetId : (props.params.billet_id) ? props.params.billet_id : -1
        };
    }

    render() {
        return(
            <div className="content-container" >
                <AppBar title="Billets" style={styleContentItem}/>
                <div style={styleContentMain}>
                    <BilletTable billetTargetId={this.state.billetTargetId} />
                </div>
            </div>
        );
    }
}

export default Billets;