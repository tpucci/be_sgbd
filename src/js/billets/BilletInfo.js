import React from 'react';
import Chip from 'material-ui/Chip';
import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Identity from 'material-ui/svg-icons/action/perm-identity';
import TrainIcon from 'material-ui/svg-icons/maps/train';
import EuroIcon from 'material-ui/svg-icons/action/euro-symbol';
import SeatIcon from 'material-ui/svg-icons/notification/airline-seat-recline-extra';
import {cyan500, green500, cyan800, grey400} from 'material-ui/styles/colors';
import Pending from 'material-ui/svg-icons/action/watch-later';
import Confirmed from 'material-ui/svg-icons/action/check-circle';
import Canceled from 'material-ui/svg-icons/navigation/cancel';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import FlatButton from 'material-ui/FlatButton';
import { browserHistory } from 'react-router';
import ActionInfo from 'material-ui/svg-icons/action/info';
import LinearProgress from 'material-ui/LinearProgress';

import '../helpers/FormatMoney';
import '../helpers/PriceRateFormat';
import del from '../helpers/Delete';
import put from '../helpers/Put';
import getDateKey from '../helpers/GetDateKey';
import getTimeKey from '../helpers/GetTimeKey';

import {setBilletState} from './BilletTable';

const styles = {
  wrapper : {
    marginTop : 15,
    display : 'flex',
    flexFlow : 'column nowrap',
    overflowX: "hidden"
  },
  divider : {
  },
  center : {
    alignSelf : 'center'
  },
  chipWrapper: {
      display: 'flex',
      flexWrap: 'nowrap',
      alignItems : 'center',
      justifyContent: 'space-between'
  },
  chip : {
  },
  chipText : {
    fontSize : "13px"
  },
  stateBox : {
    display : 'flex',
    flexFlow : 'row nowrap',
    overflow: "hidden",
    justifyContent: "space-between",
    flex: "1 0 100%"
  }
};

export default class BilletInfo extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      billet : []
    }
  }

  componentWillMount() {
    this.setState({billet: this.props.billet});
  }

  componentWillReceiveProps(nextProps) {
    this.setState({billet: nextProps.billet});
  }

  _delete() {
      del('/api/ticket/'+this.props.target).then((response) => {
          console.log(response);
          browserHistory.push('#/billets');
          location.reload();
      })
      .catch((err)=>{
          console.log("Erreur :" + err);
      });
  }

  _handleStateChange = (e, value) => {
      put('/api/ticket/'+this.props.target+'/state/'+value,{}).then((response) => {
          setBilletState(this.props.target, value);
      })
      .catch((err)=>{
          console.log("Erreur :" + err);
      });
  };

  _mapInformation() {
    let billet = this.state.billet;
    let state, stateText;
    let disabledPending = false;
    let disabledConfirmed = false;
            switch(billet.state) {
                case 'canceled':
                    state = (<Canceled style={{fill: grey400, float: "right", alignSelf: "center", width: 64, height: 64}} />);
                    stateText = (<div style={styles.center}>Annulé</div>);
                    [disabledPending, disabledConfirmed] = [true, true];
                    break;
                case 'pending':
                    state = (<Pending style={{fill: cyan800, float: "right", alignSelf: "center", width: 64, height: 64}}/>);
                    stateText = (<div style={styles.center}>En attente</div>);
                    break;
                case 'confirmed':
                    state = (<Confirmed style={{fill: green500, float: "right", alignSelf: "center", width: 64, height: 64}} />);
                    stateText = (<div style={styles.center}>Confirmé</div>);
                    disabledPending = true;
                    break;
            }
      let stateRadioGroup = (
        <RadioButtonGroup name="state" valueSelected={billet.state} style={{alignSelf: "center", width : 135}} onChange={this._handleStateChange}>
          <RadioButton
            value="pending"
            label="En attente"
            disabled = {disabledPending}
            checkedIcon={<Pending style={{fill: cyan800, float: "right", alignSelf: "center"}}/>}
          />
          <RadioButton
            value="confirmed"
            label="Confirmé"
            disabled = {disabledConfirmed}
            checkedIcon={<Confirmed style={{fill: green500, float: "right", alignSelf: "center"}} />}
          />
          <RadioButton
            value="canceled"
            label="Annulé"
            checkedIcon={<Canceled style={{fill: grey400, float: "right", alignSelf: "center"}} />}
          />
        </RadioButtonGroup>
      );
      let reduction = null;
      if (billet.reduction_name !== null) {
        reduction = (
          <ListItem
            primaryText={
              <div style={styles.chipWrapper}>
                <div>{billet.reduction_name}</div>
                <Chip style={styles.chip}>
                    <span style={styles.chipText}>{billet.reduction_price_rate.priceRateFormat()}</span>
                </Chip>
              </div>
            }
          href={"#/reductions"}
          rightIcon={<ActionInfo />}
          insetChildren={true}
          />
        );
      }
      let reduction_card = null;
      if (billet.card_name !== null) {
        reduction_card = (
          <ListItem
            primaryText={
              <div style={styles.chipWrapper}>
                <div>{billet.card_name}</div>
                <Chip style={styles.chip}>
                    <span style={styles.chipText}>{billet.reduction_card_price_rate.priceRateFormat()}</span>
                </Chip>
              </div>
            }
          href={"#/clients/"+billet.id_customer}
          rightIcon={<ActionInfo />}
          insetChildren={true}
          />
        );
      }
      let situation = "";
      switch(billet.situation) {
        case 'window':
          situation = " (fenêtre)";
          break;
        case 'aisle':
          situation = " (couloir)";
          break;
      }

    return (
      <div style={styles.wrapper}>
        <div style={styles.stateBox} className="statusBox">
          <div className="statusBoxItem">{state}{stateText}</div>
          <div>{stateRadioGroup}</div>
        </div>
        <List>
          <ListItem disabled={true}
            leftIcon={<EuroIcon color={cyan500} />}
            primaryText={billet.price.formatMoney()}
          />
          {reduction}
        </List>
        <Divider inset={true} style={styles.divider} />
        <List>
          <ListItem
            leftIcon={<Identity color={cyan500} />}
            primaryText={billet.first_name + " " + billet.last_name}
            secondaryText="Prénom Nom"
            href={"#/clients/"+billet.id_customer}
            rightIcon={<ActionInfo />}
          />
          {reduction_card}
        </List>
        <Divider inset={true} style={styles.divider} />
        <List>
          <ListItem disabled={true}
            leftIcon={<TrainIcon color={cyan500} />}
            primaryText={
                  billet.departure_station+" - "+billet.arrival_station
                }
                secondaryText={
                  getDateKey(billet.departure_time)+" "+getTimeKey(billet.departure_time)+" - "+getTimeKey(billet.arrival_time)
                }
          />
          <ListItem disabled={true}
            leftIcon={<SeatIcon color={cyan500} />}
            primaryText={
                  "Voiture "+billet.coach_num+" - Place "+billet.seat_num+situation
                }
                secondaryText={
                  "Classe "+ billet.class
                }
          />

        </List>
        <FlatButton style={styles.center} label="Supprimer" onClick={this._delete}/>
      </div>
    );
  }

  render() {
    let content = (<LinearProgress mode="indeterminate" />);
    if (this.state.billet.id > 0) {
      content = this._mapInformation();
    }
    return (
      <span>{content}</span>
    );
  }
}