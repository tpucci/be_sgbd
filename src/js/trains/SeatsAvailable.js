import React from 'react';
import SeatIcon from 'material-ui/svg-icons/action/event-seat';
import CheckIcon from 'material-ui/svg-icons/action/done';
import SvgIcon from 'material-ui/SvgIcon';
import DisatisfiedSvg from 'mdi-svg/d/emoticon-sad';
import Avatar from 'material-ui/Avatar';
import Paper from 'material-ui/Paper';
import {cyan400} from 'material-ui/styles/colors';
import AddBilletForm from './AddBilletForm';
import CircularProgress from 'material-ui/CircularProgress';

import fetch from '../helpers/Fetch';
import '../helpers/ArrayLastItem';

const textStyle = { color : "white", flex: "0 0 auto", margin: "15px", textAlign: "center"};
const elementStyle = {flex: "0 1 auto", margin: "15px"};
const style = {
    flex: "1 0 10px",
    margin: "15px",
    background : cyan400,
    display: "flex",
    flexFlow: "column nowrap",
    justifyContent: "center",
    minHeight: "200px",
    alignItems: "center"
};

const DisatisfiedIcon = (props) => (
    <SvgIcon {...props} style={{fill : style.background}}>
        <path d={DisatisfiedSvg} />
    </SvgIcon>
);

class SeatsAvailable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            nb_available_seats : -1
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.selectedSegments.length > 0) {
            this._getAvailableSeats(nextProps);
        } else {
            this.setState({nb_available_seats: -1});
        }
    }

    _getAvailableSeats(nextProps) {
        this.setState({nb_available_seats: -3});
        fetch("/api/seats/available/"+nextProps.selectedSegments[0]+"-"+nextProps.selectedSegments.last()).then((res) => {
            let result = null;
            this.setState({nb_available_seats: res.nb_available_seats});
        })
        .catch((err) => {
            console.log("Erreur : "+err);
        });
    }

    render() {
        let result = null;
        switch (this.state.nb_available_seats) {
            case -3:
                result = (
                    <Paper zDepth={1} style={style}>
                        <CircularProgress color="white"/>
                    </Paper>
                );
                break;
            case -2:
                result = (
                    <Paper zDepth={1} style={style}>
                        <Avatar icon={DisatisfiedIcon}  backgroundColor={"white"} style={elementStyle}/>
                        <p style={textStyle}>Oups !</p>
                        <p style={textStyle}>Une erreur est survenue, rechargez la page pour réessayer.</p>
                    </Paper>
                );
                break;
            case -1:
                result = (
                    <Paper zDepth={1} style={style}>
                        <Avatar icon={<CheckIcon style={{fill : style.background}}/>}  backgroundColor={"white"} style={elementStyle}/>
                        <p style={textStyle}>Sélectionner les segments pour afficher les places disponibles</p>
                    </Paper>
                );
                break;
            case 0:
                result = (
                    <Paper zDepth={1} style={style}>
                        <Avatar icon={<SeatIcon style={{fill: style.background}}/>}  backgroundColor={"white"} style={elementStyle}/>
                        <p style={textStyle}>Il n'y a plus de places disponibles pour ce trajet.</p>
                    </Paper>
                );
                break;
            default:
                result = (
                    <Paper zDepth={1} style={style}>
                        <Avatar icon={<SeatIcon style={{fill: style.background}}/>}  backgroundColor={"white"} style={elementStyle}/>
                        <p style={textStyle}>{this.state.nb_available_seats} places disponibles.</p>
                        <AddBilletForm {...this.props}/>
                    </Paper>
                );
        }
        return result;
    }
}

export default SeatsAvailable;