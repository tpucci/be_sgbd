import React from 'react';
import {ListItem} from 'material-ui/List';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow} from 'material-ui/Table';
import CityStep from './CityStep';
import TrainRow from './TrainRow';
import TrainIcon from 'material-ui/svg-icons/maps/train';
import Avatar from 'material-ui/Avatar';
import {cyan600, darkBlack} from 'material-ui/styles/colors';
import Paper from 'material-ui/Paper';
import SeatsAvailable from './SeatsAvailable';
import LinearProgress from 'material-ui/LinearProgress';

import '../helpers/ArrayLastItem';
import '../helpers/TwoDigitsString';
import '../helpers/MaxMinArray';
import fetch from '../helpers/Fetch';

export default class Train extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            segments: [],
            selectedStation : [],
            inBetween: [],
            selectedSegments: []
        };
    }

    componentWillMount() {
        this._getSegments();
    }

    _getSegments = () => {
        fetch('/api/train/'+this.props.id).then((segments) => {
            this.setState({segments: segments});
        })
        .catch((err)=>{
            console.log("Erreur :" + err);
        });        
    }

    _mapSegments = () => {
        if (this.state.segments) {
            let index = 1;
            return this.state.segments.map((segment) => {
            let arrival_date = new Date(segment.arrival_time);
            let hours = arrival_date.getHours();
            let minutes = arrival_date.getMinutes();
            index += 1;
            return(
                <CityStep key={segment.id}
                          station={segment.arrival_station}
                          hours={hours} minutes={minutes}
                          index={index}
                          selected={this.state.selectedStation.indexOf(index-1) !== -1}
                          inBetween={this.state.inBetween.indexOf(index-1) !== -1}/>
            )
            });
        }
    }

    _onRowSelection = (rows) => {
        let min = rows.min();
        let max = rows.max();
        this.setState({selectedStation : [min, max]});
        let inBetween = [];
        let selectedSegments = [];
        if ( min < max && -1 < min ) {
            for (let i = min+1; i < max; i++) {
                inBetween.push(i);
            }
            selectedSegments = [this.state.segments[min].id].concat(
                inBetween.map((stationIndex)=>{ return this.state.segments[stationIndex].id; })
            );
        }
        this.setState({
            inBetween : inBetween,
            selectedSegments : selectedSegments
        });
    }

    render() {
        let date = new Date(this.props.date);
        let hours = date.getHours();
        let minutes = date.getMinutes();
        let date_arrival = new Date(this.props.arrival_time);
        let hours_arrival = date_arrival.getHours();
        let minutes_arrival = date_arrival.getMinutes();
        let segments = this._mapSegments();
        if (segments.length == 0) segments = (<LinearProgress mode="indeterminate" />);
        const titre = this.props.departure_station + " - " + this.props.arrival_station;
        const secondaryText = (
            <p>
              <span style={{color: darkBlack}}>{hours.toTwoChar() + ":" + minutes.toTwoChar()} - {hours_arrival.toTwoChar() + ":" + minutes_arrival.toTwoChar()}</span><br />
              {"#"+this.props.id}
            </p>
        );
        const leftPaneStyle = {
            flex: "3 0 10px",
            maxWidth: "600px",
            margin: "16px"
        };
        return (
            <ListItem
                primaryText={titre}
                primaryTogglesNestedList={true}
                secondaryText={secondaryText}
                secondaryTextLines={2}
                leftAvatar={<Avatar icon={<TrainIcon />}  backgroundColor={cyan600}/>}
                nestedItems={[
                    <TrainRow key="0">
                        <Paper zDepth={1} style={leftPaneStyle}>
                            <Table multiSelectable={true}
                                   onRowSelection={this._onRowSelection}>
                                <TableHeader displaySelectAll={false}>
                                    <TableRow>
                                        <TableHeaderColumn>#</TableHeaderColumn>
                                        <TableHeaderColumn>Heure de passage</TableHeaderColumn>
                                        <TableHeaderColumn>Station</TableHeaderColumn>
                                    </TableRow>
                                </TableHeader>
                                <TableBody deselectOnClickaway={false}>
                                    <CityStep station={this.props.departure_station}
                                              hours={hours}
                                              minutes={minutes}
                                              index={1}
                                              selected={this.state.selectedStation.indexOf(0) !== -1}
                                              inBetween={this.state.inBetween.indexOf(0) !== -1}/>
                                    {segments}
                                </TableBody>
                            </Table>
                        </Paper>
                        <SeatsAvailable selectedSegments={this.state.selectedSegments} />
                    </TrainRow>
                ]}
            />

        );
    }
}