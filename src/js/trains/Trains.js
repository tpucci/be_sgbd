import React, { Component } from 'react';
import AppBar from '../components/AppBar';
import Date from './Date';

import fetch from '../helpers/Fetch';
import groupByDate from '../helpers/GroupByDate';
import LinearProgress from 'material-ui/LinearProgress';

const styleContentItem = {
    flex: "0 0 auto"
};

const styleContentMain = {
    overflowY: "scroll",
    overflowX: "hidden",
    flex: "1 1 auto"
};

let handleTrainsFilter = () => {return -1;};

class Trains extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fromStationID: -1,
            toStationID: -1,
            trains: []
        };
    }

    _getTrainsByDate = () => {
        fetch('/api/trains').then((trains) => { let trainsByDate = groupByDate(trains); this.setState({'trains': trainsByDate});})
        .catch((err)=>{
            console.log("Erreur :" + err);
        });
    }

    _mapTrains = () => {
        if (this.state.trains)
            return this.state.trains.map((dateTrains) => {
                return(
                    <Date key={dateTrains.date}
                        date={dateTrains.date}
                        trains={dateTrains.trains} />

                );
            });
    }

    componentWillMount() {
        this.setState({trains: this._getTrainsByDate()});
    }

    render() {
        let trains = this._mapTrains();
        if (!trains) trains = (<LinearProgress mode="indeterminate" />);
        return(
            <div className="content-container" >
                <AppBar title="Trains" style={styleContentItem}/>
                <div style={styleContentMain}>
                    {trains}
                </div>
            </div>
        );
    }
}

export default Trains;
export {handleTrainsFilter};