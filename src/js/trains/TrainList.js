import React from 'react';
import Date from './Date';

class TrainList extends React.Component {

    _mapTrainsByDate = () => {
        return this.props.trains.map((dateTrains) => {
            return(
                <Date key={dateTrains.date}
                      date={dateTrains.date}
                      trains={dateTrains.trains} />

            );
        });
    }

    render() {
        const trains = this._mapTrainsByDate();
        return(
            <div>
                {trains}
            </div>
        );
    }
}

export default TrainList;