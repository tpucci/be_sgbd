import React from 'react';
import {List} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import Train from './Train';

export default class Date extends React.Component {

    _getTrains(trains) {
        return trains.map((train) => {
            return(
                <Train key={train.id}
                       id={train.id}
                       date={train.departure_time}
                       arrival_time={train.arrival_time}
                       departure_station={train.departure_station}
                       arrival_station={train.arrival_station}
                       />
            );
        });
    }

    render() {
        const trains = this._getTrains(this.props.trains);
        return (
            <List>
                <Subheader>{this.props.date}</Subheader>
                {trains}
                <Divider />
            </List>
        );
    }
}
