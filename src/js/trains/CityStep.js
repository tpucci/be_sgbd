import React from 'react';
import {TableRow, TableRowColumn} from 'material-ui/Table';
import ArrowDownIcon from 'material-ui/svg-icons/navigation/arrow-drop-down-circle';
import {cyan500, grey200} from 'material-ui/styles/colors';

import '../helpers/TwoDigitsString';

export default class CityStep extends React.Component {

    render() {
        let { index, station, hours, minutes, inBetween, ...other } = this.props;
        let style = {};
        if (inBetween) {
            other.children[0] = <TableRowColumn><ArrowDownIcon style={{fill: cyan500}} /></TableRowColumn>;
            style = {background: grey200};
        }
        return (
            <TableRow {...other} style={style}>
                {other.children[0] /* checkbox passed down from Table-Body*/}
                <TableRowColumn>{index}</TableRowColumn>
                <TableRowColumn>{hours.toTwoChar()}:{minutes.toTwoChar()}</TableRowColumn>
                <TableRowColumn>{station}</TableRowColumn>
            </TableRow>
        );
    }
}
