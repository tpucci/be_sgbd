import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import AutoComplete from 'material-ui/AutoComplete';
import Checkbox from 'material-ui/Checkbox'
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import Lock from 'material-ui/svg-icons/action/lock';
import LockBorder from 'material-ui/svg-icons/action/lock-open';
import SvgIcon from 'material-ui/SvgIcon';
import oneBoxO from 'mdi-svg/d/numeric-1-box-outline';
import oneBox from 'mdi-svg/d/numeric-1-box';
import twoBoxO from 'mdi-svg/d/numeric-2-box-outline';
import twoBox from 'mdi-svg/d/numeric-2-box';
import chkBoxO from 'mdi-svg/d/checkbox-blank-outline';
import chkBox from 'mdi-svg/d/checkbox-marked';
import Avatar from 'material-ui/Avatar';
import SeatIcon from 'material-ui/svg-icons/action/event-seat';
import {
  cyan50,
  cyan500,
  red500
} from 'material-ui/styles/colors';
import Snackbar from 'material-ui/Snackbar';

import fetch from '../helpers/Fetch';
import post from '../helpers/Post';
import put from '../helpers/Put';
import '../helpers/FormatMoney';
import '../helpers/ArrayLastItem';

const oneBoxOI = (
    <SvgIcon>
        <path d={oneBoxO} />
    </SvgIcon>
);
const oneBoxI = (
    <SvgIcon>
        <path d={oneBox} />
    </SvgIcon>
);
const twoBoxOI = (
    <SvgIcon>
        <path d={twoBoxO} />
    </SvgIcon>
);
const twoBoxI = (
    <SvgIcon>
        <path d={twoBox} />
    </SvgIcon>
);
const chkBoxOI = (
    <SvgIcon>
        <path d={chkBoxO} />
    </SvgIcon>
);
const chkBoxI = (
    <SvgIcon>
        <path d={chkBox} />
    </SvgIcon>
);

const styles = {
  block: {
    maxWidth: 250,
  },
  radioButton: {
    marginBottom: 16,
  },
  container: {
      display: "flex",
      flexFlow: "row wrap",
      alignItems: "stretch",
      justifyContent: "space-around"
  },
  item: {
      flex: "0 0 50%"
  },
  avatar: {
      fontSize: 48
  },
  checkbox: {
    flex: "0 0 230px",
    width: 230
  },
  warning: {
      color: red500,
      flex: "0 0 100%"
  }
};


export default class AddBilletForm extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            open: false,
            clients : [],
            reductions : [],
            segments : this.props.segments,
            clientID : null,
            reductionID : null,
            confirmed : true,
            class: 2,
            situation: 0,
            openConfirm: false,
            openWarning: false,
            price: -1
        };
    }

    componentWillMount() {
        this._getClients();
        this._getReductions();
        this._getPrice();
    }

    _getClients = () => {
        fetch('/api/c').then((clients) => {
            let newClients = clients.map((client)=>{
                return {id: client.id, name: client.first_name + " " + client.last_name};
            });
            this.setState({'clients': newClients}); 
        })
        .catch((err)=>{
            console.log("Erreur :" + err);
        });
    };

    _getReductions = () => {
        fetch('/api/r').then((reductions) => { this.setState({'reductions': reductions}); })
        .catch((err)=>{
            console.log("Erreur :" + err);
        });
    };

    _getPrice = (options = {}) => {

        let defaults = {
            class : this.state.class,
            clientID : this.state.clientID,
            situation : this.state.situation,
            reductionID : this.state.reductionID
        }

        let settings = Object.assign({}, defaults, options);

        let url_seat = "/api/seats/available/"
        url_seat += this.props.selectedSegments[0];
        url_seat += "-";
        url_seat += this.props.selectedSegments.last();
        url_seat += "?class=";
        url_seat += settings.class;
        if (settings.situation != 0) {url_seat += "&situation="+settings.situation;}
        fetch(url_seat).then((response) => {
            if (response.nb_available_seats>0) {
                let url_price = "/api/price/";
                url_price += this.props.selectedSegments[0];
                url_price += "-";
                url_price += this.props.selectedSegments.last();
                url_price += "/";
                url_price += settings.class;
                if(settings.clientID > 0 || settings.reductionID > 0) {
                    let options = [];
                    if (settings.clientID > 0) {options.push("customer="+settings.clientID);}
                    if (settings.reductionID > 0) {options.push("reduction="+settings.reductionID);}
                    url_price += "?"+options.join("&");
                }
                fetch(url_price).then((res) => { this.setState({openWarning: false, price: res.price}); })
                .catch((err)=>{
                    console.log("Erreur :" + err);
                    this.setState({openWarning: false, price: -1});
                });
            } else {
                console.log('warning : no seat available');
                this.setState({openWarning: true, price: -1});
            }
        })
        .catch((err)=>{
            console.log("Erreur :" + err);
            this.setState({price: -1});;
        });
    }

    _setClient = (textValue, index) => {
        this.setState({clientID:this.state.clients[index].id});
        this._getPrice({clientID:this.state.clients[index].id});
    }

    _setReduction = (textValue, index) => {
        this.setState({reductionID:this.state.reductions[index].id});
        this._getPrice({reductionID:this.state.reductions[index].id});
    }

    _handleSituationChange = (e, value) => {
        this.setState({situation: value});
        this._getPrice({situation: value});
    }

    _handleClassChange = (e, value) => {
        this.setState({class: value});
        this._getPrice({class: value});
    }

    _handleOpen = () => {
        this.setState({
            open: true,
            clients : [],
            reductions : [],
            segments : this.props.segments,
            clientID : null,
            reductionID : null,
            confirmed : true,
            class: 2,
            situation: 0,
            openConfirm: false,
            openWarning: false,
            price: -1
        });
        this._getClients();
        this._getReductions();
        this._getPrice({
            class : 2,
            clientID : null,
            situation : 0,
            reductionID : null
        });
    };

    _handleClose = () => {
        this.setState({open: false});
    };

    _handleSubmit = () => {
        let params = {};
        if(this.state.clientID > 0 && this.state.price >= 0) {
            params.id_departure_segment = this.props.selectedSegments[0];
            params.id_arrival_segment = this.props.selectedSegments.last();
            params.class = this.state.class;
            params.id_customer = this.state.clientID;
            if (this.state.reductionID > 0) {console.log("ok"); params.id_reduction = this.state.reductionID;}
            if (this.state.situation != 0) {params.situation = this.state.situation;}
            post("/api/book", params).then((response) => {
                if(response.insert_id>0){
                    if (this.state.confirmed) {
                        put('/api/ticket/'+response.insert_id+'/state/confirmed',{}).then((response) => {
                            this._handleClose(); this.setState({openConfirm: true});
                        })
                        .catch((err)=>{
                            alert('Une erreur est survenue : impossible de passer le billet en statut confimé. Son état est : "En attente".');
                            console.log("Erreur :" + err);
                        });
                    } else {
                        this._handleClose(); this.setState({openConfirm: true});
                    }
                } else {
                    alert("Une erreur est survenue : le billet n'a pas été créé.");
                }
            })
            .catch((err)=>{
                console.log("Erreur :" + err);
            });
        } else {
            this.setState({openConfirm: true});
            alert("Une erreur est survenue");
        }
    }

    _handleToggleStatus = (e, bool) => {
        this.setState({confirmed: bool});
    }

    render() {

        let price = this.state.price;

        const actions = [
        <FlatButton
            label="Annuler"
            primary={true}
            onTouchTap={this._handleClose}
        />,
        <FlatButton
            label="Envoyer"
            primary={true}
            disabled={(price<0 || this.state.clientID == null)}
            onTouchTap={this._handleSubmit}
        />,
        ];

        const dataSourceConfig = {
            text: 'name',
            value: 'id',
        };


        let avatar = "";
        if (price > 0) {
            avatar = (<Avatar
                    color={cyan50}
                    backgroundColor={cyan500}
                    size={192}
                    style={styles.avatar}
                    >
                    {price.formatMoney()}
                    </Avatar>);
        } else {
            avatar = (<Avatar
                    color={cyan50}
                    backgroundColor={cyan500}
                    size={192}
                    style={styles.avatar}
                    icon={<SeatIcon style={{fill : cyan50}}/>}/>);
        }

        const lastline = (this.state.openWarning) ? (<p style={styles.warning}>Aucun siège disponible pour vos préférences</p>) : (<Checkbox
                    checkedIcon={<Lock />}
                    uncheckedIcon={<LockBorder />}
                    label={(this.state.confirmed) ? "Réservation immédiate" : "Poser une option"}
                    onCheck={this._handleToggleStatus}
                    defaultChecked={true}
                     style={styles.checkbox}
                    labelPosition="left"
                    />);

        return (
        <div style={{flex: "0 1 auto", margin: "15px"}}>
            <RaisedButton label="Réserver" onClick={this._handleOpen}/>
            <Dialog
            title="Réserver le train"
            actions={actions}
            modal={true}
            open={this.state.open}
            >
                <div style={styles.container}>
                    <div style={styles.item}>
                        <AutoComplete
                        floatingLabelText="Client"
                        filter={AutoComplete.fuzzyFilter}
                        openOnFocus={false}
                        dataSource={this.state.clients}
                        dataSourceConfig={dataSourceConfig}
                        onNewRequest={this._setClient}
                        /><br/>

                        <AutoComplete
                        floatingLabelText="Réduction"
                        filter={AutoComplete.fuzzyFilter}
                        openOnFocus={false}
                        dataSource={this.state.reductions}
                        dataSourceConfig={dataSourceConfig}
                        onNewRequest={this._setReduction}
                        />
                    </div>

                    {avatar}

                    <div  style={styles.item}>
                        <h4>Classe</h4>
                        <RadioButtonGroup name="class" defaultSelected="2" onChange={this._handleClassChange}>
                            <RadioButton
                                value="2"
                                label="2ème classe"
                                checkedIcon={twoBoxI}
                                uncheckedIcon={twoBoxOI}
                                style={styles.radioButton}
                            />
                            <RadioButton
                                value="1"
                                label="1ère classe"
                                checkedIcon={oneBoxI}
                                uncheckedIcon={oneBoxOI}
                                style={styles.radioButton}
                            />
                        </RadioButtonGroup>
                    </div>
                    
                    <div  style={styles.item}>
                        <h4>Situation</h4>
                        <RadioButtonGroup name="situation" defaultSelected="0" onChange={this._handleSituationChange}>
                            <RadioButton
                                value="0"
                                label="Sans préférence"
                                style={styles.radioButton}
                            />
                            <RadioButton
                                value="aisle"
                                label="Couloir"
                                style={styles.radioButton}
                            />
                            <RadioButton
                                value="window"
                                label="Fenêtre"
                                style={styles.radioButton}
                            />
                        </RadioButtonGroup>
                    </div>

                    {lastline}

                </div>
            </Dialog>
            <Snackbar
            open={this.state.openConfirm}
            message="Billet ajouté"
            autoHideDuration={4000}
            />
        </div>
        );
    }
}