import React from 'react';

export default class TrainRow extends React.Component {

    render() {
        return (
            <div className="train-row-container">
                {this.props.children}
            </div>
        );
    }
}
