export default function getTimeKey(dateIn) {
  let date = (new Date(dateIn))
  let h = date.getHours();
  let m = date.getMinutes();
  return ((h<10?'0':'') + h) + ':' + ((m<10?'0':'') + m);
}