import getDateKey from './GetDateKey';

export default function groupByDate (trainsObject) {
    return trainsObject.reduce(function(acc, d){
        let p = getDateKey(d.departure_time);
        if (!acc[0].hasOwnProperty(p)) acc[0][p] = [];
        acc[0][p].push(d);
        return acc;
    },[{}])
    .reduce(function(acc, v){
        Object.keys(v).forEach(function(k){acc.push({date:k, trains:v[k]})});
        return acc;
    },[]);
}