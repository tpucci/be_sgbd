Number.prototype.toTwoChar = function() {
    return ("0" + this).slice(-2);
};

export default Number.prototype.toTwoChar;