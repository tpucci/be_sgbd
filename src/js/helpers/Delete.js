export default (url) => {
    return new Promise( (resolve, reject) => {
        let request = new XMLHttpRequest();
        request.open('DELETE', url, true);
        request.onloadend = () => {
            if(request.readyState == 4 && request.status == 200) {
                resolve(JSON.parse(request.response));
            }
        };
        request.onerror = () => {
            reject(new Error("Error posting result"));
        };
        request.send();
    });
};