Number.prototype.priceRateFormat = function(){
    return "-" + Math.round((1-this)*10000)/100 + "%";
}

export default Number.prototype.priceRateFormat;