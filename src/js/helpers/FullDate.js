import toTwoChar from './TwoDigitsString';

Date.prototype.fullDate = function(){
    return this.getDate().toTwoChar()+"/"+(this.getMonth()+1).toTwoChar()+"/"+this.getFullYear();
};

Date.prototype.toDatabaseDate = function(){
    return this.getFullYear() + '-' +
    ('00' + (this.getMonth()+1)).slice(-2) + '-' +
    ('00' + this.getDate()).slice(-2);
};

Date.prototype.toDatabaseDateTime = function(){
    return this.toDatabaseDate + ' ' + 
    ('00' + this.getHours()).slice(-2) + ':' + 
    ('00' + this.getMinutes()).slice(-2) + ':' + 
    ('00' + this.getSeconds()).slice(-2);
};

export default Array.prototype.fullDate;