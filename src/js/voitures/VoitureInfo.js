import React from 'react';
import Avatar from 'material-ui/Avatar';
import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Star from 'material-ui/svg-icons/toggle/star';
import SeatIcon from 'material-ui/svg-icons/notification/airline-seat-recline-extra';
import TrainIcon from 'material-ui/svg-icons/maps/train';
import {cyan500} from 'material-ui/styles/colors';
import LinearProgress from 'material-ui/LinearProgress';

import fetch from '../helpers/Fetch';
import getDateKey from '../helpers/GetDateKey';

const styles = {
  wrapper : {
    marginTop : 15,
    display : 'flex',
    flexFlow : 'column nowrap',
    overflowX: "hidden"
  },
  divider : {
  },
  center : {
    alignSelf : 'center'
  },
  chipWrapper: {
      display: 'flex',
      flexWrap: 'nowrap',
      alignItems : 'center',
      justifyContent: 'space-between'
  },
  chip : {
  },
  chipText : {
    fontSize : "13px"
  }
};

export default class VoitureInfo extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      coach : null
    };
  }

    componentWillMount() {
      if (this.props.target > -1) {
        this._getInformation(this.props.target);
      }
    }

    componentWillReceiveProps(nextProps) {
      if (nextProps.target > -1) {
        this._getInformation(nextProps.target);
      }
    }

  _getInformation(target) {
        fetch('/api/coach/'+target).then((info) => { this.setState({'coach': info});})
        .catch((err)=>{
            console.log("Erreur : " + err);
        });
  }

  _mapInformation = () => {
    let coach = this.state.coach;
    return (
      <div style={styles.wrapper}>
        <Avatar size={64} style={styles.center}>
            {coach.coach.id}
        </Avatar>
        <List>
          <ListItem disabled={true}
            leftIcon={<Star color={cyan500} />}
            primaryText={coach.coach.class}
            secondaryText="Type"
          />
          <ListItem disabled={true}
            insetChildren={true}
            primaryText={coach.coach.capacity}
            secondaryText="Capacité"
          />
        </List>
        <Divider inset={true} style={styles.divider} />
        <List>
          {coach.seats.map((seat,index)=>{
            let prop = (index === 0) ? {leftIcon: <SeatIcon color={cyan500} /> } : {insetChildren: true};
            let situation;
            switch(seat.situation) {
              case "window":
                situation = "Fenêtre";
                break;
              case "aisle":
                situation = "Couloir";
                break;
              default:
                situation = "Autre";
            }
            return(
              <ListItem disabled={true} key={index}
                {...prop}
                primaryText={situation}
                secondaryText={
                  seat.nb_seats + " sièges"
                }
              />
            );
          })}
        </List>
        <Divider inset={true} style={styles.divider} />
        <List>
          {coach.travels.map((train,index)=>{
            if (train.departure_station != null) {
              let prop = (index === 0) ? {leftIcon: <TrainIcon color={cyan500} /> } : {insetChildren: true};
              return(
                <ListItem disabled={true} key={train.id_train}
                  {...prop}
                  primaryText={
                    train.departure_station+" - "+train.arrival_station
                  }
                  secondaryText={
                    getDateKey(train.departure_time)
                  }
                />
              );
            }
          })}
        </List>
      </div>
    );
  }

  render() {
    let content = (
      <LinearProgress mode="indeterminate" />
    );
    if (this.state.coach != null) {
      content = this._mapInformation();
    }
    return (
      <span>{content}</span>
    );
  }
}