import React from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import InformationPanel from '../components/InformationPanel';
import VoitureInfo from './VoitureInfo';
import { browserHistory } from 'react-router';
import LinearProgress from 'material-ui/LinearProgress';

import fetch from '../helpers/Fetch';

class VoitureTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            targetVoitureID : props.voitureTargetId,
            voitures : null
        };
    }

    componentWillMount() {
        this._getVoitures();
    }

    _getVoitures = () => {
        fetch('/api/coaches').then((voitures) => { this.setState({'voitures': voitures}); })
        .catch((err)=>{
            console.log("Erreur :" + err);
        });
    }

    _mapVoitures = () => {
        if (this.state.voitures) {
            return this.state.voitures.map((voiture) => {
                let coachClass = "";
                switch(voiture.class){
                    case 1:
                        coachClass = "1ère classe";
                        break;
                    case 2:
                        coachClass = "2ème classe";
                        break;
                    default:
                        coachClass = "inconnue";
                };
                return(
                    <TableRow key={voiture.id} selected={this.state.targetVoitureID == voiture.id}>
                        <TableRowColumn>{voiture.id}</TableRowColumn>
                        <TableRowColumn>{coachClass}</TableRowColumn>
                        <TableRowColumn>{voiture.capacity}</TableRowColumn>
                    </TableRow>
                );
            });
        }
    }

    _onRowSelection = (rows) => {
        if(rows.length > 0) {
            this.setState({
                targetVoitureID : this.state.voitures[rows[0]].id
            });
            browserHistory.push('#/voitures/'+this.state.voitures[rows[0]].id);
        } else {
            this.setState({
                targetVoitureID : -1
            });
            browserHistory.push('#/voitures');
        }
    }

    render() {
        let voitures = this._mapVoitures();

        if (!voitures) return (<LinearProgress mode="indeterminate" />);

        return(
            <div>
                <Table onRowSelection={this._onRowSelection}>
                    <TableHeader displaySelectAll={false}>
                        <TableRow>
                            <TableHeaderColumn>#</TableHeaderColumn>
                            <TableHeaderColumn>Classe</TableHeaderColumn>
                            <TableHeaderColumn>Capacité</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody deselectOnClickaway={false}>
                        {voitures}
                    </TableBody>
                </Table>
                <InformationPanel target={this.state.targetVoitureID}>
                    <VoitureInfo target={this.state.targetVoitureID}/>
                </InformationPanel>
            </div>
        );
    }
}

export default VoitureTable;