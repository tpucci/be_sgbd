import React, { Component } from 'react';
import AppBar from '../components/AppBar';
import VoitureTable from './VoitureTable';

const styleContentItem = {
    flex: "0 0 auto"
};

const styleContentMain = {
    overflowY: "scroll",
    overflowX: "hidden",
    flex: "1 1 auto"
};

class Voitures extends Component {

    constructor(props) {
        super(props);
        this.state = {
            voitureTargetId : (props.params.voiture_id) ? props.params.voiture_id : -1
        };
    }

    render() {
        return(
            <div className="content-container" >
                <AppBar title="Voitures" style={styleContentItem}/>
                <div style={styleContentMain}>
                    <VoitureTable voitureTargetId={this.state.voitureTargetId} />
                </div>
            </div>
        );
    }
}

export default Voitures;