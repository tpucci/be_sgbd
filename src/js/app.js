/**
 * React Components & Router
 */

import React from 'react';
import {Router, Route, hashHistory} from 'react-router';
import ReactDom from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import Main from './container';
import Trains from './trains';
import Voitures from './voitures';
import Clients from './clients';
import Reductions from './reductions';
import Billets from './billets';
import NotFound from './errorMessages/NotFound';

var $ = function(f){
    document.addEventListener('DOMContentLoaded', f, false);
};

$(()=> {

    injectTapEventPlugin();

    ReactDom.render(
        <MuiThemeProvider>
            <Main>
                <Router history={hashHistory}>
                    <Route path="/" component={Clients}/>
                    <Route path="/trains" component={Trains}/>
                    <Route path="/voitures(/:voiture_id)" component={Voitures}/>
                    <Route path="/clients(/:client_id)" component={Clients}/>
                    <Route path="/reductions" component={Reductions}/>
                    <Route path="/billets(/:billet_id)" component={Billets}/>
                    <Route path='*' component={NotFound} />
                </Router>
            </Main>
        </MuiThemeProvider>,
        document.getElementById('agence-sncf-app')
    );

});