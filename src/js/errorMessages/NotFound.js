import React, { Component } from 'react';
import AppBar from '../components/AppBar';
import Error from 'material-ui/svg-icons/alert/error-outline';
import {pink500, lightBlack} from 'material-ui/styles/colors';

const styleContentItem = {
    flex: "0 0 auto"
};

const styleContentMain = {
    overflowY: "scroll",
    overflowX: "hidden",
    flex: "1 1 auto",
    display: "flex",
    flexFlow: "column nowrap",
    alignItems: "center",
    justifyContent: "center"
};

const styleError = {
    fill: pink500,
    width: "30vh",
    height: "30vh",
    maxWidth: 250,
    maxHeight: 250,
};

const styleSubtext = {
    color: lightBlack,
    fontWeight: "300"
};

class Voitures extends Component {

    render() {
        return(
            <div className="content-container" >
                <AppBar title="Erreur" style={styleContentItem}/>
                <div style={styleContentMain}>
                    <Error style={styleError}/>
                    <h1>Oups...</h1>
                    <h3 style={styleSubtext}>Votre action n'a pas abouti.</h3>
                </div>
            </div>
        );
    }
}

export default Voitures;