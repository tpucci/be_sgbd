import React, { Component } from 'react';
import AppBar from '../components/AppBar';
import ReductionTable from './ReductionTable';

const styleContentItem = {
    flex: "0 0 auto"
};

const styleContentMain = {
    overflowY: "scroll",
    overflowX: "hidden",
    flex: "1 1 auto"
};

class Reductions extends Component {

    render() {
        return(
            <div className="content-container" >
                <AppBar title="Réductions" style={styleContentItem}/>
                <div style={styleContentMain}>
                    <ReductionTable search={null} />
                </div>
            </div>
        );
    }
}

export default Reductions;