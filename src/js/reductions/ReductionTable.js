import React from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import Avatar from 'material-ui/Avatar';
import Chip from 'material-ui/Chip';
import {indigo500, indigo200} from 'material-ui/styles/colors';
import LinearProgress from 'material-ui/LinearProgress';

import fetch from '../helpers/Fetch';
import '../helpers/PriceRateFormat';

const style = {
    wrapper: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip : {
        margin: 4,
        backgroundColor : indigo200
    },
    chipText : {
        fontSize : "13px"
    },
    chipAvatar : {
        fontSize : "13px",
        backgroundColor : indigo500
    }
};

class ReductionTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            reductions : null
        };
    }

    componentWillMount() {
        this._getReductions();
    }

    _getReductions = () => {
        fetch('/api/reductions').then((reductions) => { this.setState({'reductions': reductions}); })
        .catch((err)=>{
            console.log("Erreur :" + err);
        });
    }

    _mapReductions = () => {
        if (this.state.reductions)
            return this.state.reductions.map((reduction) => {
                return(
                    <TableRow key={reduction.id} selected={this.state.targetReductionID == reduction.id}>
                        <TableRowColumn>{reduction.id}</TableRowColumn>
                        <TableRowColumn>{reduction.name}</TableRowColumn>
                        <TableRowColumn>
                            <Chip style={style.chip}>
                                <Avatar size={32} style={style.chipAvatar}>
                                    {reduction.name.match(/\b./g).join('').slice(0,2)}
                                </Avatar>
                                <span style={style.chipText}>{reduction.price_rate.priceRateFormat()}</span>
                            </Chip>
                        </TableRowColumn>
                    </TableRow>
                );
            });
    }

    render() {
        const reductions = this._mapReductions();
        if (!reductions) return (<LinearProgress mode="indeterminate" />);

        return(
            <div>
                <Table selectable={false}>
                    <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                        <TableRow>
                            <TableHeaderColumn>#</TableHeaderColumn>
                            <TableHeaderColumn>Nom</TableHeaderColumn>
                            <TableHeaderColumn>Réduction</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody deselectOnClickaway={false} displayRowCheckbox={false}>
                        {reductions}
                    </TableBody>
                </Table>
            </div>
        );
    }
}

export default ReductionTable;