import React, { Component } from 'react';
import AppBarMUI from 'material-ui/AppBar';
import {toggleDrawer} from '../container/Main';

const AppBar = (props) => (
    <AppBarMUI
        onLeftIconButtonTouchTap={toggleDrawer}
        {...props}
    />
);

export default AppBar;