import React from 'react';
import Drawer from 'material-ui/Drawer';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import InfoIcon from 'material-ui/svg-icons/action/info';

import {toggleSecondaryDrawer} from '../container/Main';

export default class InformationPanel extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidUpdate() {
      toggleSecondaryDrawer(this.props.target > -1);
  }

  componentWillUnmount() {
      toggleSecondaryDrawer(false);
  }

  render() {
    return (
        <Drawer openSecondary={true} open={this.props.target > -1} {...this.props}>
            <AppBar iconElementLeft={<IconButton disabled={true}><InfoIcon /></IconButton>} />
            {this.props.children}
        </Drawer>
    );
  }
}