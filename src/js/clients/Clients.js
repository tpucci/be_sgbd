import React from 'react';
import AppBar from '../components/AppBar';
import ClientTable from './ClientTable';

const styleContentItem = {
    flex: "0 0 auto"
};

const styleContentMain = {
    overflowY: "scroll",
    overflowX: "hidden",
    flex: "1 1 auto"
};

class Clients extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            clientTargetId : (props.params.client_id) ? props.params.client_id : -1
        };
    }

    render() {
        return(
            <div className="content-container" >
                <AppBar title="Clients" style={styleContentItem}/>
                <div style={styleContentMain}>
                    <ClientTable clientTargetId={this.state.clientTargetId} />
                </div>
            </div>
        );
    }
}

export default Clients;