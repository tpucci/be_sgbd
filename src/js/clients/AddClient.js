import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import AutoComplete from 'material-ui/AutoComplete';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import Snackbar from 'material-ui/Snackbar';

import fetch from '../helpers/Fetch';
import post from '../helpers/Post';
import '../helpers/FullDate';

import {getClients} from './ClientTable';

export default class AddClientForm extends React.Component {

    constructor(props){
        super(props);
        this.state = {

            open: false,
            cards : [],
            first_name: "",
            last_name: "",
            birthdate: null,
            cardID : null,
            expire: null,
            openConfirm: false
        };
    }

    componentWillMount() {
        this._getCards();
    }

    _getCards = () => {
        fetch('/api/rc').then((cards) => {
            this.setState({'cards': cards}); 
        })
        .catch((err)=>{
            console.log("Erreur :" + err);
        });
    };

    _setCard = (textValue, index) => {
        this.setState({cardID:this.state.cards[index].id});
    };

    _handleOpen = () => {
        this.setState({open: true});
    };

    _handleClose = () => {
        this.setState({open: false});
    };

    _handleSubmit = () => {
        let params = {};
        params.first_name = this.state.first_name;
        params.last_name = this.state.last_name;
        params.birthdate = this.state.birthdate;
        if (this.state.cardID != null) {
            params.id_reduction_card = this.state.cardID;
            params.expiration_date = this.state.expire;
        }
        post('/api/customer', params).then((response) => {
            console.log(response);
            getClients();
            this._handleClose();
            this.setState({openConfirm: true});
        })
        .catch((err)=>{
            console.log("Erreur :" + err);
        });
    };

    _handleFirstNameChange = (e) => {
        this.setState({
            first_name: e.target.value
        });
    };

    _handleLastNameChange = (e) => {
        this.setState({
            last_name: e.target.value
        });
    };

    _handleBirthdateChange = (undefined, date) => {
        this.setState({
            birthdate: date.toDatabaseDate()
        });
    };

    _handleExpireChange = (undefined, date) => {
        console.log(date.toDatabaseDate());
        this.setState({
            expire: date.toDatabaseDate()
        });
    };


    _verifyForm() {
        if (this.state.first_name != "" && this.state.last_name != "" && this.state.birthdate != null) {
            return !(this.state.cardID != null && this.state.expire == null)
        } else {
            return false;
        }
    }

    render() {
        
        const actions = [
        <FlatButton
            label="Annuler"
            primary={true}
            onTouchTap={this._handleClose}
        />,
        <FlatButton
            label="Envoyer"
            primary={true}
            disabled={!this._verifyForm()}
            onTouchTap={this._handleSubmit}
        />,
        ];

        const dataSourceConfig = {
            text: 'name',
            value: 'id',
        };

        return (
        <div style={{position: "absolute", bottom: 40, right: 40}}>
            <FloatingActionButton onClick={this._handleOpen} >
                <ContentAdd />
            </FloatingActionButton>
            <Dialog
            title="Ajouter un client"
            actions={actions}
            modal={true}
            open={this.state.open}
            >

            <TextField
            floatingLabelText="Prénom"
            onChange={this._handleFirstNameChange}
            />

            <TextField
            floatingLabelText="Nom"
            onChange={this._handleLastNameChange}
            />

            <DatePicker hintText="Date de naissance"
            onChange={this._handleBirthdateChange}
            cancelLabel="Annuler"/>
            
            <AutoComplete
            floatingLabelText="Carte de réduction"
            filter={AutoComplete.fuzzyFilter}
            openOnFocus={true}
            dataSource={this.state.cards}
            dataSourceConfig={dataSourceConfig}
            onNewRequest={this._setCard}
            />
            <DatePicker hintText="Date d'expiration"
            onChange={this._handleExpireChange}
            disabled={this.state.cardID == null}
            cancelLabel="Annuler"/>
            
            
            </Dialog>
            <Snackbar
            open={this.state.openConfirm}
            message="Client ajouté"
            autoHideDuration={4000}
            />
        </div>
        );
    }
}