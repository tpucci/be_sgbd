import React from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import InformationPanel from '../components/InformationPanel';
import ClientInfo from './ClientInfo';
import Avatar from 'material-ui/Avatar';
import Chip from 'material-ui/Chip';
import AddClient from './AddClient';
import LinearProgress from 'material-ui/LinearProgress';

import { browserHistory } from 'react-router';

import fetch from '../helpers/Fetch';
import '../helpers/FullDate';

let getClients = () => {return -1};

const style = {
    wrapper: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip : {
        margin: 4
    },
    chipText : {
        fontSize : "13px"
    }
};

class ClientTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            targetClientID : props.clientTargetId,
            clients : []
        };
    }

    _getClients = () => {
        fetch('/api/customers').then((clients) => { this.setState({'clients': clients}); })
        .catch((err)=>{
            console.log("Erreur :" + err);
        });
    };

    _mapClients = () => {
        return this.state.clients.map((client) => {
            let card = "";
            if (client.card_name) {
                card = (
                    <Chip style={style.chip}>
                        <Avatar size={32} style={style.chipText}>
                            {client.card_name.match(/\b\w/g).join('')}
                        </Avatar>
                        <span style={style.chipText}>{client.card_name}</span>
                    </Chip>
                );
            }
            let birthday = new Date(client.birthdate);
            return(
                <TableRow key={client.id} selected={this.state.targetClientID == client.id}>
                    <TableRowColumn>{client.id}</TableRowColumn>
                    <TableRowColumn>{client.first_name}</TableRowColumn>
                    <TableRowColumn>{client.last_name}</TableRowColumn>
                    <TableRowColumn>{birthday.fullDate()}</TableRowColumn>
                    <TableRowColumn>{card}</TableRowColumn>
                </TableRow>
            );
        });
    };

    _onRowSelection = (rows) => {
        if(rows.length > 0) {
            this.setState({
                targetClientID: this.state.clients[rows[0]].id
            });
            browserHistory.push('#/clients/'+this.state.clients[rows[0]].id);
        } else {
            this.setState({targetClientID: -1});
            browserHistory.push('#/clients');
        }
    };
    
    componentWillMount () {
        this._getClients();
        getClients = () => {this._getClients()};
    }

    render() {
        let clients = this._mapClients();
        if (clients.length == 0) return (<LinearProgress mode="indeterminate" />);
        return(
            <div>
                <Table onRowSelection={this._onRowSelection}>
                    <TableHeader displaySelectAll={false}>
                        <TableRow>
                            <TableHeaderColumn>#</TableHeaderColumn>
                            <TableHeaderColumn>Prénom</TableHeaderColumn>
                            <TableHeaderColumn>Nom</TableHeaderColumn>
                            <TableHeaderColumn>Date de naissance</TableHeaderColumn>
                            <TableHeaderColumn>Carte de fidélité</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody deselectOnClickaway={false}>
                        {clients}
                    </TableBody>
                </Table>
                <InformationPanel target={this.state.targetClientID} width={512}>
                    <ClientInfo target={this.state.targetClientID} />
                </InformationPanel>
                <AddClient/>
            </div>
        );
    }
}

export default ClientTable;
export {getClients};