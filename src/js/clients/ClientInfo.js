import React from 'react';
import Avatar from 'material-ui/Avatar';
import Chip from 'material-ui/Chip';
import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Identity from 'material-ui/svg-icons/action/perm-identity';
import CardIcon from 'material-ui/svg-icons/action/card-membership';
import BilletIcon from 'material-ui/svg-icons/action/receipt';
import {cyan500} from 'material-ui/styles/colors';
import FlatButton from 'material-ui/FlatButton';
import { browserHistory } from 'react-router';
import ActionInfo from 'material-ui/svg-icons/action/info';
import LinearProgress from 'material-ui/LinearProgress';

import fetch from '../helpers/Fetch';
import del from '../helpers/Delete';
import '../helpers/PriceRateFormat';
import getDateKey from '../helpers/GetDateKey';
import getTimeKey from '../helpers/GetTimeKey';

const styles = {
  wrapper : {
    marginTop : 15,
    display : 'flex',
    flexFlow : 'column nowrap',
    overflowX: "hidden"
  },
  divider : {
  },
  center : {
    alignSelf : 'center'
  },
  chipWrapper: {
      display: 'flex',
      flexWrap: 'nowrap',
      alignItems : 'center',
      justifyContent: 'space-between'
  },
  chip : {
  },
  chipText : {
    fontSize : "13px"
  }
};

export default class ClientInfo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          client: null,
          billets: null
        };
    }

    componentWillMount() {
      if (this.props.target > -1) {
        this._getInformation(this.props.target);
      }
    }

    componentWillReceiveProps(nextProps) {
      if (nextProps.target > -1) {
        this._getInformation(nextProps.target);
      }
    }

    _getInformation(target) {
        fetch('/api/customer/'+target).then((result) => { this.setState({'client': result.customer, 'billets': result.tickets}); })
        .catch((err)=>{
            console.log("Erreur :" + err);
        });
    }

    _delete = () => {
        del('/api/customer/'+this.props.target).then((response) => {
            console.log(response);
            browserHistory.push('#/clients');
            location.reload();
        })
        .catch((err)=>{
            console.log("Erreur :" + err);
        });
    };

  _mapInformation = () => {
    let client = this.state.client;
    let billets = this.state.billets;
    let birthday = new Date(client.birthdate);
    let card = "";
    if (client.card_name) {
        card = (
            <div>
                <List>
                    <ListItem disabled={true}
                    leftIcon={<CardIcon color={cyan500} style={{top: 8}}/>}
                    primaryText={
                        <div style={styles.chipWrapper}>
                        <div>
                            <div>{client.card_name}</div>
                        </div>
                        <Chip style={styles.chip}>
                            <span style={styles.chipText}>{client.price_rate.priceRateFormat()}</span>
                        </Chip>
                        </div>
                    }
                    />
                </List>
                <Divider inset={true} style={styles.divider} />
            </div>
        );
    };

    return (
      <div style={styles.wrapper}>
        <Avatar size={64} style={styles.center}>
            {client.first_name.match(/\b[A-Z]/g).join('')+client.last_name.match(/\b[A-Z]/g).join('')}
        </Avatar>
        <List>
          <ListItem disabled={true}
            leftIcon={<Identity color={cyan500} />}
            primaryText={client.first_name + " " + client.last_name}
            secondaryText="Prénom Nom"
          />
          <ListItem disabled={true}
            insetChildren={true}
            primaryText={birthday.fullDate()}
            secondaryText="Date de naissance"
          />
        </List>
        <Divider inset={true} style={styles.divider} />
        {card}
        <List>
          {billets.map((billet,index)=>{
            let prop = (index === 0) ? {leftIcon: <BilletIcon color={cyan500} /> } : {insetChildren: true};
            return(
              <ListItem key={billet.id_ticket}
                {...prop}
                primaryText={
                  billet.departure_station+" - "+billet.arrival_station
                }
                secondaryText={
                  getDateKey(billet.departure_time)+" "+getTimeKey(billet.departure_time)+" - "+getTimeKey(billet.arrival_time)
                }
                href={"#/billets/"+billet.id_ticket}
                rightIcon={<ActionInfo />}
              />
            );
          })}
        </List>
        <FlatButton style={styles.center} label="Supprimer" onClick={this._delete}/>
      </div>
    );
  }

  render() {
    let content = (<LinearProgress mode="indeterminate" />);
    if (this.state.client != null) {
      content = this._mapInformation();
    }
    return (
      <span>{content}</span>
    );
  }
}