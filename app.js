/**
 * Created by thomas on 23/11/16.
 */

'use strict';

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var pool = require('./mysqlpool');

app.use(bodyParser.json());

app.use(express.static('public'));
app.use('/api', require('./api'));

let serverPort = process.env.PORT || 8080;
app.listen(serverPort, function () {
    console.log('Application started on port ' + serverPort);
});
